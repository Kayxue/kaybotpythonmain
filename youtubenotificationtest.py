import discord
import feedparser
import asyncio
from discord.ext import commands
from core.classes import *


class YoutubeNotification(Cog_Extension):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        async def interval():
            await self.bot.wait_until_ready()
            self.channel = self.bot.get_channel(647764963376627713)
            self.yt_videoid = ""
            while not self.bot.is_close():
                entrys = feedparser.parse(
                    "https://www.youtube.com/feeds/videos.xml?channel_id=UC9aEBg7DwquKFGx5akEyH3A")
                entry=entrys.entries[0]
                newvideoid = entry['yt_videoid']
                link = entry['link']
                if self.yt_videoid == "":
                    self.yt_videoid = newvideoid
                else:
                    if not newvideoid == self.yt_videoid:
                        await self.channel.send(f"嘿！**Kay Xue學昀楷**發布了新影片，快去看！\n{link}")
                        self.yt_videoid = newvideoid
                await asyncio.sleep(1)

        self.bg_task = self.bot.loop.create_task(interval())


def setup(bot):
    bot.add_cog(YoutubeNotification(bot))
