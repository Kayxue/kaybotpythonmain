import pymongo
import dns
import json

with open("JSON/setting.json", mode="r", encoding="utf8")as jfile:
    jdata = json.load(jfile)

client = pymongo.MongoClient(
    jdata['MongoDBWebsite']
)
allserversetting = client['KayBotPython']['serversetting']

for i in allserversetting.find():
    try:
        x = i['levelmessagechannel']
    except KeyError:
        allserversetting.update_one({"_id": str(i["_id"])}, {"$set": {"levelmessagechannel": "未設定"}})
