import discord
import random
import json
import os
import asyncio
from discord.ext import commands
from core.classes import *
from disputils import *

with open('JSON/setting.json', 'r', encoding='utf8') as jfile:
    jdata = json.load(jfile)


class Bot(commands.Bot):
    def __init__(self):
        super(Bot, self).__init__(command_prefix=[
            's!', 'sesame '], help_command=None, description="由芝麻湯圓所製作的機器人")
        self.add_cog(maintask(self))

    async def changeactivity(self):
        await self.wait_until_ready()
        while not self.is_closed():
            await self.change_presence(status=discord.Status.online, activity=discord.Game(name="s!help / sesame help"))
            await asyncio.sleep(5)
            await self.change_presence(status=discord.Status.online, activity=discord.Game(name=f"{len(self.guilds)}個伺服器觀看中"))
            await asyncio.sleep(5)
            await self.change_presence(status=discord.Status.online, activity=discord.Game(name=f"為{len(self.users)}個成員服務中"))
            await asyncio.sleep(5)

    async def on_ready(self):
        self.loop.create_task(Bot.changeactivity(self))
        print('Bot is online')


class maintask(Cog_Extension):

    def is_it_me(ctx):
        return ctx.author.id == 470516498050580480

    @commands.command(pass_context=True)
    @commands.check(is_it_me)
    async def load(self, ctx, extension):
        bot.load_extension(f'apps.{extension}')
        embed1 = discord.Embed(
            title='指定類別指令載入成功！', description=f'成功載入{extension}類別指令！', color=0xc8f4fb)
        await ctx.send(embed=embed1)

    @load.error
    async def load_error(self, ctx, error):
        if isinstance(error, discord.ext.commands.CheckFailure):
            embed1 = discord.Embed(
                title='權限不足！', description='只有湯圓本人能執行此指令喔！', color=0xc8f4fb)
            await ctx.channel.send(embed=embed1)
        elif isinstance(error, discord.ext.commands.MissingRequiredArgument):
            embed1 = discord.Embed(
                title='請輸入要載入之類別！', description='不會用嗎？沒關係，我幫你', color=0xc8f4fb)
            embed1.add_field(name='用法', value='``s!load [類別名稱]``')
            embed1.add_field(name='範例', value='``s!load event``')
            await ctx.channel.send(embed=embed1)
        else:
            embed1 = discord.Embed(
                title='載入失敗！', description='對不起，無法載入該類別之指令', color=0xc8f4fb)
            embed1.add_field(name='錯誤碼', value=error, inline=False)
            await ctx.channel.send(embed=embed1)

    @commands.command()
    @commands.check(is_it_me)
    async def unload(self, ctx, extension):
        bot.unload_extension(f'apps.{extension}')
        embed1 = discord.Embed(
            title='指定類別指令解除載入成功！', description=f'成功解除載入{extension}類別指令！', color=0xc8f4fb)
        await ctx.send(embed=embed1)

    @unload.error
    async def unload_error(self, ctx, error):
        if isinstance(error, discord.ext.commands.CheckFailure):
            embed1 = discord.Embed(
                title='權限不足！', description='只有湯圓本人能執行此指令喔！', color=0xc8f4fb)
            await ctx.channel.send(embed=embed1)
        elif isinstance(error, discord.ext.commands.MissingRequiredArgument):
            embed1 = discord.Embed(title='請輸入要解除載入之類別！',
                                   description='不會用嗎？沒關係，我幫你', color=0xc8f4fb)
            embed1.add_field(name='用法', value='``s!unload [類別名稱]``')
            embed1.add_field(name='範例', value='``s!unload event``')
            await ctx.channel.send(embed=embed1)
        else:
            embed1 = discord.Embed(
                title='解除載入失敗！', description='對不起，無法解除載入該類別之指令', color=0xc8f4fb)
            embed1.add_field(name='錯誤碼', value=error, inline=False)
            await ctx.channel.send(embed=embed1)

    @commands.command()
    @commands.check(is_it_me)
    async def reload(self, ctx, extension):
        bot.reload_extension(f'apps.{extension}')
        embed1 = discord.Embed(
            title='已重新載入指定類別指令！', description=f'成功重新載入{extension}類別指令！', color=0xc8f4fb)
        await ctx.send(embed=embed1)

    @reload.error
    async def reload_error(self, ctx, error):
        if isinstance(error, discord.ext.commands.CheckFailure):
            embed1 = discord.Embed(
                title='權限不足！', description='只有湯圓本人能執行此指令喔！', color=0xc8f4fb)
            await ctx.channel.send(embed=embed1)
        elif isinstance(error, discord.ext.commands.MissingRequiredArgument):
            embed1 = discord.Embed(title='請輸入要重新載入之類別！',
                                   description='不會用嗎？沒關係，我幫你', color=0xc8f4fb)
            embed1.add_field(name='用法', value='``s!reload [類別名稱]``')
            embed1.add_field(name='範例', value='``s!reload event``')
            await ctx.channel.send(embed=embed1)
        else:
            embed1 = discord.Embed(
                title='重新載入失敗！', description='對不起，不明原因，無法重新載入該類別之指令', color=0xc8f4fb)
            embed1.add_field(name='錯誤碼', value=error, inline=False)
            await ctx.channel.send(embed=embed1)

    @commands.command()
    async def help(self, ctx):
        embed = [discord.Embed(title='幫助－資訊類（1/2）', description='資訊類指令表', color=0xc8f4fb)
                 .add_field(name='help', value='顯示此列表', inline=False)
                 .add_field(name='botinfo', value='查看關於此機器人之資訊', inline=False)
                 .add_field(name="support", value='加入支援伺服器', inline=False)
                 .add_field(name='userinfo', value='顯示出你的使用者資訊', inline=False)
                 .add_field(name='couponleft', value='查詢三倍券剩餘數量', inline=False),
                 discord.Embed(title='幫助－資訊類（2/2）',
                               description='資訊類指令表', color=0xc8f4fb)
                 .add_field(name='showtime', value='顯示當前所在時區之時間', inline=False)
                 .add_field(name='weather', value='查詢指定地點的天氣', inline=False)
                 .add_field(name='maskleft', value='查詢指定藥局口罩剩餘數量', inline=False)
                 .add_field(name='youbikeleft', value='查詢Youbike站點剩餘車輛', inline=False),
                 discord.Embed(
                     title='幫助－伺服器資訊類', description='伺服器資訊類指令表，\n所有人能執行這些指令喔！', color=0xc8f4fb)
                 .add_field(name='serverinfo', value='查看此伺服器的資訊', inline=False)
                 .add_field(name='serverboost', value='查看此伺服器之加速狀態', inline=False),
                 discord.Embed(title='幫助－伺服器訊息管理類',
                               description='伺服器訊息管理類指令表，\n要執行此類指令必須要有「管理訊息」權限', color=0xc8f4fb)
                 .add_field(name='clear', value='清理指定數量之訊息', inline=False)
                 .add_field(name='clearuserallmessage', value='清理使用者在此伺服器發送的所有訊息', inline=False)
                 .add_field(name='commandcleanup', value='清理指定指令開頭之訊息', inline=False),
                 discord.Embed(
                     title='幫助－其他伺服器管理類', description='伺服器管理類指令表，\n要執行此類指令必須要有該指令相對應之權限', color=0xc8f4fb)
                 .add_field(name='kick', value='將指定使用者踢出伺服器\n（必須權限：踢出成員）', inline=False)
                 .add_field(name='ban', value='將指定使用者封鎖\n（必須權限：封鎖成員）', inline=False),
                 discord.Embed(title='幫助－建議類（暫停使用）',
                               description="給予建議", color=0xc8f4fb)
                 .add_field(name="makesuggestion", value="提出一個建議", inline=False)
                 .add_field(name="approve", value="批准一個建議", inline=False)
                 .add_field(name="implemented", value="將該建議批改結果改為已實施", inline=False)
                 .add_field(name="consider", value="將該建議批改結果改為思考中", inline=False)
                 .add_field(name="deny", value="將該建議批改結果改為已拒絕", inline=False),
                 discord.Embed(title="幫助－邀請類",
                               description="管理邀請", color=0xc8f4fb)
                 .add_field(name="createinvite", value="機器人幫你創造一個邀請", inline=False)
                 .add_field(name="listinvite", value="列出此伺服器已建立之邀請", inline=False),
                 discord.Embed(title="幫助－挑戰類（暫停使用）",
                               description="建立挑戰", color=0xc8f4fb)
                 .add_field(name="makechallenge", value="創造一個挑戰", inline=False)
                 .add_field(name="challengeinfo", value="顯示關於此挑戰之資訊", inline=False)
                 .add_field(name="endchallenge", value="結束一個挑戰", inline=False),
                 discord.Embed(title="幫助－休閒娛樂類（1/2）",
                               description="休閒與娛樂", color=0xc8f4fb)
                 .add_field(name="say", value="讓bot說出指定訊息", inline=False)
                 .add_field(name="saydel", value="讓bot說出指定訊息（輸入之指令會被刪除）", inline=False)
                 .add_field(name="nitroemoji", value="讓bot送出別的伺服器擁有之表情符號", inline=False)
                 .add_field(name="nitroemojidel", value="讓bot送出別的伺服器擁有之表情符號（輸入之指令會被刪除）", inline=False),
                 discord.Embed(title="幫助－休閒娛樂類（2/2）",
                               description="休閒與娛樂", color=0xc8f4fb)
                 .add_field(name="choose", value="讓bot讓你做選擇", inline=False)
                 .add_field(name="random", value="隨機在指定區間選一個整數", inline=False)
                 .add_field(name="maketeam", value="將目前狀態為線上之成員隨機分隊", inline=False)
                 .add_field(name="loli", value="隨機顯示蘿莉圖", inline=False),
                 discord.Embed(title="幫助－等級與貨幣",
                               description="查看你的等級與貨幣", color=0xc8f4fb)
                 .add_field(name="rank", value="顯示等級卡", inline=False),
                 discord.Embed(title="幫助－機器人設定",
                               description="機器人在此伺服器的設定", color=0xc8f4fb)
                 .add_field(name="setchannel", value="設定機器人各功能之訊息發送頻道", inline=False)
                 .add_field(name="setmention", value="設定機器人在發指定訊息時會提及什麼身分組", inline=False),
                 discord.Embed(
                     title='幫助－機器人管理類', description='機器人管理類指令表，\n僅湯圓本人能執行這些指令喔！\n這些為管理機器人載入之功能與指令的指令', color=0xc8f4fb)
                 .add_field(name='load', value='啟用指定類別之功能或指令', inline=False)
                 .add_field(name='reload', value='重新載入指定類別之功能或指令', inline=False)
                 .add_field(name='unload', value='關閉指定類別之功能或指令', inline=False),
                 discord.Embed(
                     title="幫助－敬請期待", description="指令尚未完成，輸入時只會得到敬請期待訊息", color=0xc8f4fb)
                 .add_field(name="setpoststar", value="設定一個訊息被多少用戶附加星號反應時會將該訊息貼至排行榜", inline=False)
                 .add_field(name="giveaway", value="舉辦一個抽獎", inline=False),
                 ]
        paginator = BotEmbedPaginator(ctx, embed)
        await paginator.run()

    @commands.command()
    async def help2(self, ctx, type=None):
        menupageoneemoji = ["1️⃣", "2️⃣", "3️⃣", "4️⃣", "5️⃣", "⏹"]
        menupagetwoemoji = ["6️⃣", "7️⃣", "8️⃣", "9️⃣", "⏹"]
        categoryembedemoji = ["🔼"]
        menuembed = [discord.Embed(title="選擇指令類別")
                     .add_field(name="1️⃣一般資訊類", value="生活資訊與機器人資訊")
                     .add_field(name="2️⃣伺服器資訊類", value="關於此伺服器的資訊")
                     .add_field(name="3️⃣伺服器管理類", value="管理此伺服器")
                     .add_field(name="4️⃣建議類", value="給予建議")
                     .add_field(name="5️⃣邀請類", value="管理邀請"),
                     discord.Embed(title="選擇指令類別")
                     .add_field(name="6️⃣挑戰類", value="建立挑戰")
                     .add_field(name="7️⃣休閒娛樂類", value="休閒與娛樂")
                     .add_field(name="8️⃣等級與貨幣", value="查看你的等級與貨幣")
                     .add_field(name="9️⃣機器人設定", value="機器人在此伺服器的設定")]
        categoryembed = [discord.Embed(title="一般資訊類")
                         .add_field(name="help", value="顯示機器人指令求助列表")
                         .add_field(name="botinfo", value="顯示")
                         .add_field(name="userinfo", value="")
                         .add_field(name="weather", value="")
                         .add_field(name="showtime", value=""),
                         discord.Embed(title="伺服器資訊類")
                         .add_field(name="serverinfo", value="顯示此伺服器的資訊")
                         .add_field(name="serverboost", value="顯示此伺服器的加成狀態"),
                         discord.Embed(title="伺服器管理類")
                         .add_field(name="clear", value="清除指定數量之訊息")
                         .add_field(name="kick", value="踢除成員")
                         .add_field(name="ban", value="封鎖成員"),
                         discord.Embed(title="建議類")
                         .add_field(name="makesuggestion", value="提出一個建議"),
                         discord.Embed(title="邀請類")
                         .add_field(name="createinvite", value="機器人幫你創造一個邀請")
                         .add_field(name="listinvite", value="列出此伺服器已建立之邀請"),
                         discord.Embed(title="挑戰類")
                         .add_field(name="makechallenge", value="創造一個挑戰")
                         .add_field(name="challengeinfo", value="顯示關於此挑戰之資訊")
                         .add_field(name="endchallenge", value="結束一個挑戰"),
                         discord.Embed(title="休閒娛樂類")
                         .add_field(name="say", value="讓bot說出指定訊息")
                         .add_field(name="saydel", value="讓bot說出指定訊息（輸入之指令會被刪除）")
                         .add_field(name="loli", value="隨機顯示蘿莉圖")
                         .add_field(name="maketeam", value="將目前狀態為線上之成員隨機分隊")
                         .add_field(name="choose", value="讓bot讓你做選擇")
                         .add_field(name="randomint", value="隨機在指定區間選一個整數")
                         .add_field(name="nitroemoji", value="讓bot送出別的伺服器擁有之表情符號")
                         .add_field(name="nitroemojidel", value="讓bot送出別的伺服器擁有之表情符號（輸入之指令會被刪除）"),
                         discord.Embed(title="等級與貨幣")
                         .add_field(name="rank", value="顯示等級卡"),
                         discord.Embed(title="機器人設定")
                         .add_field(name="configchannel", value="設定機器人各功能之訊息發送頻道")
                         .add_field(name="configmention", value="設定機器人在發指定訊息時會提及什麼身分組")]
        menupage = 0
        outcategory = -1
        outmessage = await ctx.channel.send(embed=menuembed[menupage])
        for i in menupageoneemoji:
            await outmessage.add_reaction(i)

        def check(reaction, user):
            if outcategory == -1:
                if menupage == 0:
                    return user == ctx.author and str(reaction.emoji) in menupageoneemoji
                elif menupage == 1:
                    return user == ctx.author and str(reaction.emoji) in menupagetwoemoji
            else:
                return user == ctx.author and str(reaction.emoji) in categoryembedemoji
        while True:
            try:
                reaction, user = await self.bot.wait_for('reaction_add', timeout=20, check=check)
                if outcategory == -1:
                    if menupage == 0:
                        for i in menupageoneemoji:
                            await outmessage.add_reaction(i)
                    elif menupage == 1:
                        for i in menupagetwoemoji:
                            await outmessage.add_reaction(i)
            except asyncio.TimeoutError:
                await outmessage.clear_reactions()
                return
            else:
                if outcategory == -1:
                    if menupage == 0:
                        if menupageoneemoji.index(str(reaction.emoji)) != 5:
                            outcategory = menupageoneemoji.index(
                                str(reaction.emoji))
                        else:
                            await outmessage.delete()
                            return
                    elif menupage == 1:
                        if menupagetwoemoji.index(str(reaction.emoji)) != 4:
                            outcategory = (menupagetwoemoji.index(
                                str(reaction.emoji)))+6
                        else:
                            await outmessage.delete()
                            return
                    await outmessage.clear_reactions()
                    await outmessage.edit(embed=categoryembed[outcategory])
                    for i in categoryembedemoji:
                        await outmessage.add_reaction(i)
                else:
                    outcategory = -1
                    await outmessage.clear_reactions()
                    await outmessage.edit(embed=menuembed[menupage])


bot = Bot()

for filename in os.listdir('./apps'):
    if filename.endswith('.py'):
        bot.load_extension(f'apps.{filename[:-3]}')

if __name__ == "__main__":
    bot.run(jdata["TOKEN"])
