import discord
from discord.ext import commands


class Cog_Extension(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.normalembedcolor = 0xc8f4fb
        self.errorembedcolor = 0xff625c
        self.successfulembedcolor = 0x98ffa0
