import pymongo
import dns
import json
with open("JSON/setting.json", mode="r", encoding="utf8")as jfile:
    jdata = json.load(jfile)
client = pymongo.MongoClient(jdata['MongoDBWebsite'])
allserversetting = client['KayBotPython']['serversetting']


class ReturnLanguage():
    def returnLanguageAlreadyReadDatabase(self, databasedic: dict, findfile: str, category: str):
        with open(f"JSON/{databasedic['language']}/{findfile}.json", mode="r", encoding="utf8") as jfile:
            jdata = json.load(jfile)
        return jdata[category]

    def returnLanguageNotReadDatabase(self, ctx, findfile: str, category: str):
        serverdata = allserversetting.find_one({"_id": str(ctx.guild.id)})
        with open(f"JSON/{serverdata['language']}/{findfile}.json", mode="r", encoding="utf8") as jfile:
            jdata = json.load(jfile)
        return jdata[category]
