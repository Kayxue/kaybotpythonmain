import discord
from discord.ext import commands
from core.classes import *
from typing import Union
import pymongo
import json

with open("JSON/setting.json", mode="r", encoding="utf8") as jfile:
    jdata = json.load(jfile)

client = pymongo.MongoClient(jdata['MongoDBWebsite'])
reactionrolesetting = client['KayBotPython']['reactionrole']


class ReactionRole(Cog_Extension):
    @commands.command()
    async def addreactionrole(self, ctx, inchannel: Union[discord.TextChannel, int], inmessage: int, inreaction: str, inrole: Union[discord.Role, int]):
        message = inmessage
        reaction = inreaction
        role = inrole
        channel = inchannel
        aftersplit = []
        reaction = str(reaction)
        reaction = reaction.replace("<", "")  # 去頭
        reaction = reaction.replace(">", "")  # 去尾
        aftersplit = reaction.split(":")

        print(reaction)

        if len(aftersplit) != 1:  # 如果是官方表情
            del aftersplit[0]

        print(aftersplit)

        if len(aftersplit) == 2:
            if len(aftersplit[1]) > 18:  # 多個表情符號但都是伺服器內
                await ctx.channel.send("一次只能選一個表情！")
                return

        if len(aftersplit) > 2:  # 多個表情符號但是其中有包含內建
            await ctx.channel.send("一次只能選一個表情！")
            return

        if isinstance(channel, discord.TextChannel):  # 提及頻道
            try:
                message = await channel.fetch_message(message)
            except discord.NotFound:
                await ctx.channel.send("找不到該則訊息！")
                return
        else:  # 輸入頻道ID
            channel = discord.utils.get(ctx.guild.channels, id=channel)
            if channel == None:  # 頻道找不到
                await ctx.channel.send("請輸入正確頻道ID！")
                return
            try:
                message = await channel.fetch_message(message)
            except discord.NotFound:
                await ctx.channel.send("找不到該則訊息！")
                return
        if isinstance(role, int):
            role = discord.utils.get(ctx.guild.roles, id=role)
            if role == None:
                await ctx.channel.send("請輸入正確身分組ID！")
                return
        if len(aftersplit) > 1:
            emoji = discord.utils.get(ctx.guild.emojis, name=aftersplit[0])
            if emoji == None:
                await ctx.channel.send("我看不到該表情！")
                return
        else:
            emoji = aftersplit[0]
        if reactionrolesetting.find_one({"_id": str(ctx.guild.id)+str(message.id)+aftersplit[0], "channel": channel.id, "message": message.id, "emoji": aftersplit[0]}) == None:
            reactionrolesetting.insert_one({"_id": str(ctx.guild.id)+str(
                message.id)+aftersplit[0], "channel": channel.id, "message": message.id, "emoji": aftersplit[0], "role": role.id})
        else:
            await ctx.channel.send("請勿重複新增！")
            return
        print(type(emoji))
        await message.add_reaction(emoji)
        await ctx.channel.send("新增完畢")

    @addreactionrole.error
    async def addreactionrole_error(self, ctx, error):
        embed1 = discord.Embed(
            title="執行失敗！", description="對不起無法執行！", color=self.normalembedcolor)
        embed1.add_field(name="錯誤碼", value=error)
        await ctx.channel.send(embed=embed1)

    @commands.Cog.listener()
    async def on_raw_reaction_add(self, payload):
        if not self.bot.get_user(payload.user_id) == self.bot.user:
            print(str(payload.emoji))
            reaction = str(payload.emoji)
            reaction = reaction.replace("<", "")  # 去頭
            reaction = reaction.replace(">", "")  # 去尾
            aftersplit = reaction.split(":")

            if len(aftersplit) != 1:  # 如果是官方表情
                del aftersplit[0]

            data = reactionrolesetting.find_one({"_id": str(
                payload.guild_id)+str(payload.message_id)+str(aftersplit[0]), "channel": payload.channel_id, "message": payload.message_id, "emoji": aftersplit[0]})
            if not data == None:
                guild = self.bot.get_guild(payload.guild_id)
                member = guild.get_member(payload.user_id)
                role = discord.utils.get(guild.roles, id=data['role'])
                if not role == None:
                    await member.add_roles(role)

    @commands.Cog.listener()
    async def on_raw_reaction_remove(self, payload):
        if not self.bot.get_user(payload.user_id) == self.bot.user:
            print(str(payload.emoji))
            reaction = str(payload.emoji)
            reaction = reaction.replace("<", "")  # 去頭
            reaction = reaction.replace(">", "")  # 去尾
            aftersplit = reaction.split(":")

            if len(aftersplit) != 1:  # 如果是官方表情
                del aftersplit[0]

            data = reactionrolesetting.find_one({"_id": str(
                payload.guild_id)+str(payload.message_id)+str(aftersplit[0]), "channel": payload.channel_id, "message": payload.message_id, "emoji": aftersplit[0]})
            if not data == None:
                guild = self.bot.get_guild(payload.guild_id)
                member = guild.get_member(payload.user_id)
                role = discord.utils.get(guild.roles, id=data['role'])
                if not role == None:
                    await member.remove_roles(role)


def setup(bot):
    bot.add_cog(ReactionRole(bot))
