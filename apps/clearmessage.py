import discord
from discord.ext import commands
from core.classes import *
from typing import Union
from core.returnlanguage import *


class ClearMessage(Cog_Extension, ReturnLanguage):
    @commands.command()
    @commands.has_permissions(manage_messages=True)
    async def clear(self, ctx, num: int):
        languagedic = self.returnLanguageNotReadDatabase(
            ctx, "clearmessage", "clear")
        await ctx.channel.purge(limit=num + 1)
        embed1 = discord.Embed(
            title=languagedic['embedtitle'],
            description=languagedic['embedcontent'].format(number=num),
            color=self.successfulembedcolor)
        await ctx.channel.send(embed=embed1)

    @clear.error
    async def clear_error(self, ctx, error):
        languagedic = self.returnLanguageNotReadDatabase(
            ctx, "clearmessage", "clearerror")
        if isinstance(error, discord.ext.commands.CheckFailure):
            embed1 = discord.Embed(
                title=languagedic['embedtitle']['nopermission'],
                description=languagedic['embeddescription']['nopermission'],
                color=self.errorembedcolor)
            embed1.add_field(name=languagedic['embedfield1name']['nopermission'],
                             value=languagedic['embedfield1value']['nopermission'])
            await ctx.channel.send(embed=embed1)
        elif isinstance(error, discord.ext.commands.MissingRequiredArgument):
            embed1 = discord.Embed(
                title=languagedic['embedtitle']['missingarguments'],
                description=languagedic['embeddescription']['missingarguments'],
                color=self.normalembedcolor)
            embed1.add_field(name=languagedic['embedfield1name']['missingarguments'],
                             value=languagedic['embedfield1value']['missingarguments'])
            embed1.add_field(
                name=languagedic['embedfield2name'], value=languagedic['embedfield2value'])
            await ctx.channel.send(embed=embed1)
        else:
            embed1 = discord.Embed(
                title='清除失敗！',
                description='對不起，無法清除',
                color=self.errorembedcolor)
            embed1.add_field(name='錯誤訊息：', value=error)
            await ctx.channel.send(embed=embed1)

    @commands.command()
    @commands.has_permissions(manage_messages=True)
    async def clearuserallmessage(self, ctx, user: Union[discord.Member, int]):
        messagetodelete = []
        if isinstance(user, discord.Member):
            outmessage = await ctx.channel.send(
                embed=discord.Embed(
                    title="執行指令中......",
                    description="進度：即將開始......",
                    color=self.normalembedcolor))
            finish = 1
            for chkchannel in ctx.guild.text_channels:
                try:
                    async for message in chkchannel.history():
                        if message.author == user:
                            messagetodelete.append(message)
                except discord.errors.HTTPException:
                    pass
                if finish % 2 == 0:
                    await outmessage.edit(
                        embed=discord.Embed(
                            title="執行指令中......",
                            description=f"進度：正在搜尋（{finish}/{len(ctx.guild.text_channels)}）",
                            color=self.normalembedcolor))
                finish += 1
            await message.edit(
                embed=discord.Embed(
                    title="執行指令中......", description="進度：清理訊息中......"),
                color=self.normalembedcolor)
            if not (len(messagetodelete) == 0):
                for message in messagetodelete:
                    await message.delete()
                    if finish % 4 == 0:
                        await outmessage.edit(
                            embed=discord.Embed(
                                title="執行指令中......",
                                description=f"進度：清理訊息中({finish}/{len(messagetodelete)})",
                                color=self.normalembedcolor))
                    finish += 1
        elif isinstance(user, int):
            deluser = await self.bot.fetch_user(user)
            outmessage = await ctx.channel.send(
                embed=discord.Embed(
                    title="執行指令中......",
                    description="進度：即將開始......",
                    color=self.normalembedcolor))
            finish = 1
            if deluser == None:
                embed1 = discord.Embed(
                    title="請輸入正確使用者ID！", description="對不起！無法執行！", color=self.errorembedcolor)
                await outmessage.edit(embed=embed1)
                return
            for chkchannel in ctx.guild.text_channels:
                try:
                    async for message in chkchannel.history():
                        if message.author == deluser:
                            messagetodelete.append(message)
                except discord.errors.HTTPException:
                    pass
                if finish % 2 == 0:
                    await outmessage.edit(
                        embed=discord.Embed(
                            title="執行指令中......",
                            description=f"進度：正在搜尋（{finish}/{len(ctx.guild.text_channels)}）",
                            color=self.normalembedcolor))
                finish += 1
            finish = 1
            await outmessage.edit(
                embed=discord.Embed(
                    title="執行指令中......",
                    description="進度：清理訊息中......",
                    color=self.normalembedcolor))
            if not len(messagetodelete) == 0:
                for message in messagetodelete:
                    await message.delete()
                    if finish % 4 == 0:
                        await outmessage.edit(
                            embed=discord.Embed(
                                title="執行指令中......",
                                description=f"進度：清理訊息中({finish}/{len(messagetodelete)})",
                                color=self.normalembedcolor))
                    finish += 1
        await outmessage.edit(
            embed=discord.Embed(
                title="執行指令",
                description="執行完畢",
                color=self.successfulembedcolor))

    @clearuserallmessage.error
    async def clearuserallmessage_error(self, ctx, error):
        if isinstance(error, discord.ext.commands.CheckFailure):
            embed1 = discord.Embed(
                title='權限不足！',
                description='您沒有權限執行此指令！',
                color=self.errorembedcolor)
            embed1.add_field(name='請確認您是否有以下權限：', value='管理訊息')
            await ctx.channel.send(embed=embed1)
        elif isinstance(error, discord.ext.commands.BadArgument):
            embed1 = discord.Embed(
                title='執行失敗！',
                description='對不起，找不到此使用者',
                color=self.errorembedcolor)
            await ctx.channel.send(embed=embed1)
        elif isinstance(error, discord.ext.commands.MissingRequiredArgument):
            embed1 = discord.Embed(
                title='請輸入指定使用者！',
                description='不會用嗎？沒關係，我幫你',
                color=self.normalembedcolor)
            embed1.add_field(
                name='用法', value='``s!cleanuserallmessage [成員ID/提及]``')
            embed1.add_field(
                name='範例',
                value='``s!cleanuserallmessage 983274927947329844``')
            await ctx.channel.send(embed=embed1)
        else:
            embed1 = discord.Embed(
                title='清除失敗！',
                description='對不起，無法清除',
                color=self.errorembedcolor)
            embed1.add_field(name='錯誤訊息：', value=error)
            await ctx.channel.send(embed=embed1)

    @commands.command()
    @commands.has_permissions(manage_messages=True)
    async def commandcleanup(self, ctx, prefix: str):
        messagetodelete = []
        outmessage = await ctx.channel.send(
            embed=discord.Embed(
                title="執行指令中......",
                description="進度：即將開始......",
                color=self.normalembedcolor))
        finish = 1
        for chkchannel in ctx.guild.text_channels:
            try:
                async for message in chkchannel.history():
                    if message.content.startswith(prefix):
                        messagetodelete.append(message)
            except discord.errors.HTTPException:
                pass
            if finish % 2 == 0:
                await outmessage.edit(
                    embed=discord.Embed(
                        title="執行指令中......",
                        description=f"進度：正在搜尋（{finish}/{len(ctx.guild.text_channels)}）",
                        color=self.normalembedcolor))
            finish += 1
        finish = 1
        await outmessage.edit(
            embed=discord.Embed(
                title="執行指令中......",
                description="進度：清理訊息中......",
                color=self.normalembedcolor))
        if not len(messagetodelete) == 0:
            for message in messagetodelete:
                await message.delete()
                if finish % 4 == 0:
                    await outmessage.edit(
                        embed=discord.Embed(
                            title="執行指令中......",
                            description=f"進度：清理訊息中({finish}/{len(messagetodelete)})",
                            color=self.normalembedcolor))
                finish += 1
        await outmessage.edit(
            embed=discord.Embed(
                title="執行指令", description="執行完畢", color=self.normalembedcolor))

    @commandcleanup.error
    async def commandcleanup_error(self, ctx, error):
        if isinstance(error, discord.ext.commands.CheckFailure):
            embed1 = discord.Embed(
                title='權限不足！',
                description='您沒有權限執行此指令！',
                color=self.errorembedclor)
            embed1.add_field(name='請確認您是否有以下權限：', value='管理訊息')
            await ctx.channel.send(embed=embed1)
        elif isinstance(error, discord.ext.commands.MissingRequiredArgument):
            embed1 = discord.Embed(
                title='請輸入指定之開頭！',
                description='不會用嗎？沒關係，我幫你',
                color=self.normalembedcolor)
            embed1.add_field(name='用法', value='``s!commandcleanup [指令開頭]``')
            embed1.add_field(name='範例', value='``s!commandcleanup !``')
            await ctx.channel.send(embed=embed1)


def setup(bot):
    bot.add_cog(ClearMessage(bot))
