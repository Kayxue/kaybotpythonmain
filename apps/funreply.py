import discord
from discord.ext import commands
from core.classes import *
import json
import random

with open('JSON/setting.json', 'r', encoding='utf8') as jfile:
    jdata = json.load(jfile)

with open('JSON/serversettings.json', 'r', encoding='utf8') as jfile:
    serversettings = json.load(jfile)


class FunReply(Cog_Extension):
    @commands.Cog.listener()
    async def on_message(self, message):
        serverid = str(message.guild.id)
        try:
            if not message.author.bot:
                if message.author != self.bot.user:
                    if str(message.content) == "晚安":
                        if message.channel.id == int(serversettings[str(serverid)]['funchat']):
                            if serversettings[serverid]['gamequestion'] == True or serversettings[serverid]['normalquestion'] == True:
                                serversettings[serverid]['gamequestion'] = False
                                serversettings[serverid]['normalquestion'] = False
                                with open('serversettings.json', mode='w', encoding='utf8') as finish:
                                    json.dump(
                                        serversettings, finish, sort_keys=True, indent=4, ensure_ascii=False)
                            await message.channel.send(f"{message.author.mention}：晚安")
                        else:
                            await message.channel.send(f'晚安！')
                    elif str(message.content) == "各位晚安":
                        await message.channel.send(f'晚安！{message.author.mention}！')
                    elif str(message.content) == "早安":
                        if message.channel.id == int(serversettings[str(serverid)]['funchat']):
                            if serversettings[serverid]['gamequestion'] == True or serversettings[serverid]['normalquestion'] == True:
                                serversettings[serverid]['gamequestion'] = False
                                serversettings[serverid]['normalquestion'] = False
                                with open('serversettings.json', mode='w', encoding='utf8') as finish:
                                    json.dump(
                                        serversettings, finish, sort_keys=True, indent=4, ensure_ascii=False)
                            await message.channel.send(f"{message.author.mention}：早安")
                        else:
                            await message.channel.send(f'早安！')
                    elif str(message.content) == "各位早安":
                        await message.channel.send(f'早安！{message.author.mention}！')
                    elif str(message.content) == "嗨" or str(message.content) == "Hi" or str(message.content) == "Hello":
                        if message.channel.id == int(serversettings[str(serverid)]['funchat']):
                            if serversettings[serverid]['gamequestion'] == True or serversettings[serverid]['normalquestion'] == True:
                                serversettings[serverid]['gamequestion'] = False
                                serversettings[serverid]['normalquestion'] = False
                                with open('serversettings.json', mode='w', encoding='utf8') as finish:
                                    json.dump(
                                        serversettings, finish, sort_keys=True, indent=4, ensure_ascii=False)
                            await message.channel.send(f'{message.author.mention}：嗨')
                        else:
                            await message.channel.send('嗨')
                    elif message.channel.id == int(serversettings[str(serverid)]['funchat']):
                        if not(message.content.startswith('s!')):
                            if serversettings[serverid]['gamequestion'] == False and serversettings[serverid]['normalquestion'] == False:
                                typeout = random.choice(
                                    jdata['QUESTIONORMESSAGE'])
                                if typeout == 'QUESTIONNORMAL':
                                    serversettings[serverid]['normalquestion'] = True
                                    serversettings[serverid]['gamequestion'] = False
                                    with open('serversettings.json', mode='w', encoding='utf8') as finish:
                                        json.dump(
                                            serversettings, finish, sort_keys=True, indent=4, ensure_ascii=False)
                                    outputmsg = random.choice(
                                        jdata[f'{typeout}'])
                                    await message.channel.send(f'{message.author.mention}：{outputmsg}')
                                elif typeout == 'QUESTIONGAME':
                                    serversettings[serverid]['gamequestion'] = True
                                    serversettings[serverid]['normalquestion'] = False
                                    with open('serversettings.json', mode='w', encoding='utf8') as finish:
                                        json.dump(
                                            serversettings, finish, sort_keys=True, indent=4, ensure_ascii=False)
                                    outputmsg = random.choice(
                                        jdata[f'{typeout}'])
                                    await message.channel.send(f'{message.author.mention}：{outputmsg}')
                                elif typeout == 'RANDOMMESSAGE':
                                    serversettings[serverid]['gamequestion'] = False
                                    serversettings[serverid]['normalquestion'] = False
                                    with open('serversettings.json', mode='w', encoding='utf8') as finish:
                                        json.dump(
                                            serversettings, finish, sort_keys=True, indent=4, ensure_ascii=False)
                                    outputmsg = random.choice(
                                        jdata[f'{typeout}'])
                                    await message.channel.send(f'{message.author.mention}：{outputmsg}')
                            elif serversettings[serverid]['gamequestion'] == True and serversettings[serverid]['normalquestion'] == False:
                                serversettings[serverid]['gamequestion'] = False
                                serversettings[serverid]['normalquestion'] = False
                                with open('serversettings.json', mode='w', encoding='utf8') as finish:
                                    json.dump(
                                        serversettings, finish, sort_keys=True, indent=4, ensure_ascii=False)
                                outputmsg = random.choice(jdata['ANSWERGAME'])
                                await message.channel.send(f'{message.author.mention}：{outputmsg}')
                            elif serversettings[serverid]['gamequestion'] == False and serversettings[serverid]['normalquestion'] == True:
                                serversettings[serverid]['gamequestion'] = False
                                serversettings[serverid]['normalquestion'] = False
                                with open('serversettings.json', mode='w', encoding='utf8') as finish:
                                    json.dump(
                                        serversettings, finish, sort_keys=True, indent=4, ensure_ascii=False)
                                outputmsg = random.choice(
                                    jdata['ANSWERNORMAL'])
        except KeyError:
            pass

    @commands.command()
    async def debugevent(self, ctx):
        serverid = str(ctx.guild.id)
        await ctx.channel.send(f"是否正回答遊戲問題：{serversettings[serverid]['gamequestion']}")
        await ctx.channel.send(f"是否正回答一般問題：{serversettings[serverid]['normalquestion']}")


def setup(bot):
    bot.add_cog(FunReply(bot))
