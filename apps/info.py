import discord
from discord.ext import commands
from core.classes import *
from datetime import *
import pyowm
import asyncio
import datetime
import pandas
import requests
from typing import *
from core.returnlanguage import *

owm = pyowm.OWM("512f3f992f903e7611a229fcc2c06f53")
reg = owm.city_id_registry()


class Info(Cog_Extension, ReturnLanguage):
    '''Bot資訊'''
    @commands.command(aliases=['binfo'])
    async def botinfo(self, ctx):
        emoji = discord.utils.get(self.bot.emojis, name="sesameinformation")
        embed1 = discord.Embed(title=f"{emoji} 關於此機器人", description='**Q:**我有什麼功能？\n**A:**管理伺服器、伺服器建議、娛樂休閒和生活資訊\n\n**歡 迎 邀 請 我 為 您 經營/管理 的 伺 服 器 服 務！**\n\n :point_down: 想邀請我嗎？ :point_down: \n' +
                               '[點擊此處即可邀請喔！](https://discordapp.com/api/oauth2/authorize?client_id=616940948491862028&permissions=8&scope=bot)', color=self.normalembedcolor)
        embed1.add_field(name='擁有者：', value="芝麻湯圓")
        embed1.add_field(name='主要協助：', value='Harry the Gamer - AzureX1212')
        embed1.add_field(name='discord.py版本：',
                         value=f"{discord.__version__}({discord.version_info[3]})")
        allguild = self.bot.guilds
        outguild = ""
        for i in allguild:
            outguild += f"{i.name}\n"
        embed1.add_field(name='目前所在伺服器', value=outguild, inline=False)
        await ctx.channel.send(embed=embed1)

    '''時間資訊'''
    @commands.command()
    async def showtime(self, ctx):
        outtime = datetime.datetime.now().strftime('%Y/%m/%d %p %I:%M:%S %Z')
        await ctx.channel.send(outtime)

    @commands.command(aliases=['uinfo'])
    async def userinfo(self, ctx, showuser: discord.Member = None):
        if showuser == None:
            embed1 = discord.Embed(
                title='使用者資訊', description='關於此使用者的資訊', color=self.normalembedcolor)
            embed1.add_field(name='使用者名稱：', value=ctx.author, inline=False)
            embed1.add_field(
                name='在此群之暱稱：', value=ctx.author.nick, inline=False)
            if str(ctx.author.status) == 'dnd':
                emoji = discord.utils.get(self.bot.emojis, name="dnd")
                embed1.add_field(name='狀態：', value=emoji, inline=False)
            elif str(ctx.author.status) == 'idle':
                emoji = discord.utils.get(self.bot.emojis, name="idle")
                embed1.add_field(name='狀態：', value=emoji, inline=False)
            elif str(ctx.author.status) == 'online':
                emoji = discord.utils.get(self.bot.emojis, name="online")
                embed1.add_field(
                    name='狀態：', value=emoji, inline=False)
            elif str(ctx.author.status) == 'offline':
                emoji = discord.utils.get(self.bot.emojis, name="offline")
                embed1.add_field(
                    name='狀態：', value=emoji, inline=False)
            embed1.add_field(name='此伺服器之最高身份組：',
                             value=ctx.author.top_role.mention, inline=False)
            embed1.add_field(name='加入此伺服器日期：', value=ctx.author.joined_at.strftime(
                '%Y/%m/%d %p %I:%M:%S %Z'), inline=False)
            embed1.add_field(name='帳戶創建日期：', value=ctx.author.created_at.strftime(
                '%Y/%m/%d %p %I:%M:%S %Z'), inline=False)
            embed1.set_thumbnail(url=ctx.author.avatar_url)
            embed1.set_footer(
                text=f"{ctx.author}\nID：{ctx.author.id}", icon_url=ctx.author.avatar_url)
        else:
            embed1 = discord.Embed(
                title='使用者資訊', description=f'關於使用者「{showuser.name}」的資訊', color=self.normalembedcolor)
            embed1.add_field(name='使用者名稱：', value=showuser, inline=False)
            embed1.add_field(name='在此群之暱稱：', value=showuser.nick, inline=False)
            if str(showuser.status) == 'dnd':
                embed1.add_field(name='狀態：', value='不要打擾我喔~~', inline=False)
            elif str(showuser.status) == 'idle':
                embed1.add_field(name='狀態：', value='悠閒中~~', inline=False)
            elif str(showuser.status) == 'online':
                embed1.add_field(
                    name='狀態：', value='在線中，趕快找我聊天吧！', inline=False)
            elif str(showuser.status) == 'offline':
                embed1.add_field(
                    name='狀態：', value='Sorry，我不在線，晚點喔！', inline=False)
            embed1.add_field(name='此伺服器之最高身份組：',
                             value=showuser.top_role.mention, inline=False)
            embed1.add_field(name='加入此伺服器日期：', value=showuser.joined_at.strftime(
                '%Y/%m/%d %p %I:%M:%S %Z'), inline=False)
            embed1.add_field(name='帳戶創建日期：', value=showuser.created_at.strftime(
                '%Y/%m/%d %p %I:%M:%S %Z'), inline=False)
            embed1.set_thumbnail(url=showuser.avatar_url)
            embed1.set_footer(
                text=f"{showuser}\nID：{showuser.id}", icon_url=showuser.avatar_url)
        await ctx.channel.send(embed=embed1)

    @commands.command()
    async def weather(self, ctx, *, location: str):
        locationlist = reg.ids_for(location)
        reactionemote = ["1️⃣", "2️⃣", "3️⃣", "4️⃣",
                         "5️⃣", "6️⃣", "7️⃣", "8️⃣", "9️⃣", "🔟"]
        reactiononmessage = []
        if len(locationlist) > 1:
            embed1 = discord.Embed(title="查詢結果", color=self.normalembedcolor)
            embed1.set_footer(icon_url="https://pbs.twimg.com/profile_images/1173919481082580992/f95OeyEW_400x400.jpg",
                              text="資料提供：openweathermap.org\n選擇時間：十秒")
            endj = 1
            for i in locationlist:
                embed1.add_field(
                    name=f'{reactionemote[endj-1]} 編號：{i[0]}', value=f"城市名：{i[1]}\n國碼：{i[2]}")
                endj += 1
            choose = await ctx.channel.send("請稍等......")

            startj = 0
            while startj < endj-1:
                await choose.add_reaction(reactionemote[startj])
                reactiononmessage.append(reactionemote[startj])
                startj += 1

            await choose.edit(content="", embed=embed1)

            def check(reaction, user):
                return user == ctx.author and str(reaction.emoji) in reactiononmessage

            try:
                reaction, user = await self.bot.wait_for('reaction_add', timeout=10.0, check=check)
            except asyncio.TimeoutError:
                await choose.clear_reactions()
            else:
                obs = owm.weather_at_id(
                    locationlist[reactionemote.index(str(reaction.emoji))][0])
                w = obs.get_weather()
                weather = w.get_detailed_status()
                temp = w.get_temperature(unit='celsius')
                embed1 = discord.Embed(
                    title="天氣資訊", description="該地天氣資訊如下：", color=self.normalembedcolor)
                embed1.add_field(name="天氣", value=weather, inline=False)
                embed1.add_field(name="目前溫度", value=f"{temp['temp']}\u2103")
                embed1.add_field(
                    name="最高溫度", value=f"{temp['temp_max']}\u2103")
                embed1.add_field(
                    name="最低溫度：", value=f"{temp['temp_min']}\u2103")
                embed1.set_footer(
                    icon_url="https://pbs.twimg.com/profile_images/1173919481082580992/f95OeyEW_400x400.jpg", text="資料提供：openweathermap.org")
                await choose.clear_reactions()
                await choose.edit(embed=embed1)
        elif len(locationlist) == 1:
            obs = owm.weather_at_id(locationlist[0][0])
            w = obs.get_weather()
            weather = w.get_detailed_status()
            temp = w.get_temperature(unit='celsius')
            embed1 = discord.Embed(
                title="天氣資訊", description="該地天氣資訊如下：", color=self.normalembedcolor)
            embed1.add_field(name="天氣", value=weather)
            embed1.add_field(name="目前溫度", value=f"{temp['temp']}\u2103")
            embed1.add_field(name="最高溫度", value=f"{temp['temp_max']}\u2103")
            embed1.add_field(name="最低溫度：", value=f"{temp['temp_min']}\u2103")
            embed1.set_footer(
                icon_url="https://pbs.twimg.com/profile_images/1173919481082580992/f95OeyEW_400x400.jpg", text="資料提供：openweathermap.org")
            await ctx.channel.send(embed=embed1)
        else:
            await ctx.channel.send("沒有結果喔！")

    @weather.error
    async def weathererror(self, ctx, error):
        if isinstance(error, discord.ext.commands.MissingRequiredArgument):
            embed1 = discord.Embed(
                title='請輸入要查詢之地點！', description='不會用嗎？沒關係，我幫你', color=self.normalembedcolor)
            embed1.add_field(
                name='用法', value='``s!weather [地區]``', inline=True)
            embed1.add_field(
                name='範例', value='``s!weather taipei``', inline=True)
            await ctx.channel.send(embed=embed1)
        else:
            embed1 = discord.Embed(
                title='輸出失敗！', description='對不起，無法執行！', color=self.errorembedcolor)
            embed1.add_field(name='錯誤訊息：', value=error)
            await ctx.channel.send(embed=embed1)

    @commands.command(aliases=['sp'])
    async def support(self, ctx):
        embed1 = discord.Embed(
            title="支持與幫助！", description="如果你喜歡此機器人或者需要幫助，歡迎加入下列支援伺服器！", color=self.normalembedcolor)
        embed1.add_field(name="亞洲機器人", value="暫不開放")
        embed1.add_field(name="New DL/RS/MC Chatroom",
                         value="[點我加入](https://discord.gg/9p28zrW)")
        await ctx.channel.send(embed=embed1)

    @commands.command()
    async def maskleft(self, ctx, name: str):
        message = await ctx.channel.send("查詢中請稍後.......")
        try:
            read = pandas.read_csv(
                'https://data.nhi.gov.tw/resource/mask/maskdata.csv')
            read.set_index('醫事機構名稱', inplace=True)
            mask_data = read.loc[name]
            # 抓取資訊
            address = mask_data["醫事機構地址"]
            telephone = mask_data["醫事機構電話"]
            adult_mask = int(mask_data['成人口罩剩餘數'])
            child_mask = int(mask_data['兒童口罩剩餘數'])
            update_time = mask_data['來源資料時間']
            # Embed
            embed = discord.Embed(
                title=name, description="資訊來源：data.gov.tw", color=self.normalembedcolor)
            embed.add_field(name="地址", value=address, inline=False)
            embed.add_field(name="電話", value=telephone, inline=False)
            embed.add_field(name="剩餘口罩數量 `成人/兒童`",
                            value=f"`{adult_mask}`/`{child_mask}`", inline=False)
            embed.set_footer(text=f"更新時間：{update_time}")
            await message.edit(content="感謝黑洞提供程式碼", embed=embed)
        except:
            error = f"找不到關於`{name}`的口罩資訊。"
            await message.edit(content=error)

    @maskleft.error
    async def masklefterror(self, ctx, error):
        if isinstance(error, discord.ext.commands.MissingRequiredArgument):
            embed1 = discord.Embed(
                title='請輸入要查詢之藥局或衛生機構名稱！', description='不會用嗎？沒關係，我幫你', color=self.normalembedcolor)
            embed1.add_field(
                name='用法', value='``s!maskleft [藥局或醫療機構名稱]``', inline=True)
            embed1.add_field(
                name='範例', value='``s!maskleft 松山健康服務中心``', inline=True)
            await ctx.channel.send(embed=embed1)
        else:
            embed1 = discord.Embed(
                title='輸出失敗！', description='對不起，無法執行！', color=self.errorembedcolor)
            embed1.add_field(name='錯誤訊息：', value=error)
            await ctx.channel.send(embed=embed1)

    @commands.command()
    async def youbikeleft(self, ctx, area: str, place: str):
        if area == "新北市":
            outmessage = await ctx.channel.send("查詢中請稍後......")
            outdata = requests.get(
                "https://data.ntpc.gov.tw/api/v1/rest/datastore/382000000A-000352-001").text
            if outdata[11] == "t":
                outdata = outdata.replace("t", "T", 1)
            elif outdata[11] == "f":
                outdata = outdata.replace("f", "F", 1)
            outdata = eval(outdata)
            dataout = ""
            if outdata['success']:
                for data in outdata['result']['records']:
                    if data['sna'] == place:
                        dataout = data
                if not (dataout == ""):
                    updatey = dataout['mday'][0:4]
                    updatem = dataout['mday'][4:6]
                    updated = dataout['mday'][6:8]
                    updateh = dataout['mday'][8:10]
                    updatemin = dataout['mday'][10:12]
                    updates = dataout['mday'][12:14]
                    embed1 = discord.Embed(
                        title=f"查詢YouBike站點：{dataout['sna']}", description=f"站點編號：{dataout['sno']}", color=self.normalembedcolor)
                    if dataout['act'] == "1":
                        embed1.add_field(name="營運狀態：", value="營運中")
                    else:
                        embed1.add_field(name="營運狀態：", value="未營運")
                    embed1.add_field(name=f"地址：", value=f"``{dataout['ar']}``")
                    embed1.add_field(
                        name="總停車格數：", value=f"``{dataout['tot']}``")
                    embed1.add_field(
                        name="可借車數（可借／總共）：", value=f"``{dataout['sbi']}／{dataout['tot']}``")
                    embed1.add_field(
                        name="可還車數：", value=f"``{dataout['bemp']}``")
                    embed1.set_footer(icon_url="https://upload.wikimedia.org/wikipedia/commons/thumb/e/e9/New_Taipei_City_Government_Logo.svg/1200px-New_Taipei_City_Government_Logo.svg.png",
                                      text=f"資料提供：新北市政府（https://data.ntpc.gov.tw/）\n更新時間：{updatey}/{updatem}/{updated} {updateh} : {updatemin} : {updates}")
                    await outmessage.edit(content="", embed=embed1)
                else:
                    await outmessage.edit(content="沒有結果喔！")
            else:
                await outmessage.edit("資料庫目前無資料喔！")
        elif area == "台北市":
            outmessage = await ctx.channel.send("查詢中請稍後......")
            outdata = requests.get(
                "https://tcgbusfs.blob.core.windows.net/blobyoubike/YouBikeTP.json").text
            outdata = eval(outdata)
            dataout = ""
            x = 0
            if outdata['retCode'] == 1:
                for i in range(1, 405):
                    if i < 10:
                        x = '000'+str(i)
                    elif i >= 10 and i < 100:
                        x = '00'+str(i)
                    else:
                        x = '0'+str(i)
                    try:
                        if outdata['retVal'][x]['sna'] == place:
                            dataout = outdata['retVal'][x]
                    except KeyError:
                        pass
                if not (dataout == ""):
                    updatey = dataout['mday'][0:4]
                    updatem = dataout['mday'][4:6]
                    updated = dataout['mday'][6:8]
                    updateh = dataout['mday'][8:10]
                    updatemin = dataout['mday'][10:12]
                    updates = dataout['mday'][12:14]
                    embed1 = discord.Embed(
                        title=f"查詢YouBike站點：{dataout['sna']}", description=f"站點編號：{dataout['sno']}", color=self.normalembedcolor)
                    if str(dataout['act']) == "1":
                        embed1.add_field(name="營運狀態：", value="營運中")
                    else:
                        embed1.add_field(name="營運狀態：", value="未營運")
                    embed1.add_field(name=f"地址：", value=f"``{dataout['ar']}``")
                    embed1.add_field(
                        name="總停車格數：", value=f"``{dataout['tot']}``")
                    embed1.add_field(
                        name="可借車數（可借／總共）：", value=f"``{dataout['sbi']}／{dataout['tot']}``")
                    embed1.add_field(
                        name="可還車數：", value=f"``{dataout['bemp']}``")
                    embed1.set_footer(icon_url="https://soil.taipei/Taipei/Main/images/logo-circle-50-w.png",
                                      text=f"資料提供：台北市政府（https://www.gov.taipei/）\n更新時間：{updatey}/{updatem}/{updated} {updateh} : {updatemin} : {updates}")
                    await outmessage.edit(content="", embed=embed1)
                else:
                    await outmessage.edit(content="沒有結果喔！")
            else:
                await outmessage.edit("資料庫目前無資料喔！")
        else:
            embed1 = discord.Embed(
                title='請輸入要查詢之Youbike站點的縣市！', description='不會用嗎？沒關係，我幫你', color=self.normalembedcolor)
            embed1.add_field(
                name='用法', value='``s!youbikeleft [縣市] [站點名稱]``', inline=True)
            embed1.add_field(
                name='範例', value='``s!youbikeleft 新北市 捷運新店站``', inline=True)
            embed1.add_field(
                name='縣市選項', value='**新北市**：搜尋新北市站點', inline=False)
            await ctx.channel.send(embed=embed1)

    @youbikeleft.error
    async def youbikelefterror(self, ctx, error):
        if isinstance(error, discord.ext.commands.MissingRequiredArgument):
            embed1 = discord.Embed(
                title='請輸入要查詢之Youbike站點！', description='不會用嗎？沒關係，我幫你', color=self.normalembedcolor)
            embed1.add_field(
                name='用法', value='``s!youbikeleft [縣市] [站點名稱]``', inline=True)
            embed1.add_field(
                name='範例', value='``s!youbikeleft 新北市 捷運新店站``', inline=True)
            embed1.add_field(
                name='縣市選項', value='**新北市**：搜尋新北市站點', inline=False)
            await ctx.channel.send(embed=embed1)
        else:
            embed1 = discord.Embed(
                title='輸出失敗！', description='對不起，無法執行！', color=self.errorembedcolor)
            embed1.add_field(name='錯誤訊息：', value=error)
            await ctx.channel.send(embed=embed1)


def setup(bot):
    bot.add_cog(Info(bot))
