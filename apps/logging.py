import discord
import json
import random
import datetime
import os
import pymongo
from discord.ext import commands
from core.classes import *

with open("JSON/setting.json", mode="r", encoding="utf8") as jfile:
    jdata = json.load(jfile)

client = pymongo.MongoClient(
    jdata['MongoDBWebsite'])
allserversetting = client['KayBotPython']['serversetting']


class Logging(Cog_Extension):
    @commands.Cog.listener()
    async def on_member_join(self, member):
        jdata = allserversetting.find_one({"_id": str(member.guild.id)})
        if not jdata == None:
            if not jdata['welcomechannel'] == "未設定":
                channel = self.bot.get_channel(
                    int(jdata['welcomechannel']))
                embed1 = discord.Embed(
                    title='歡迎新成員！', description=f'歡迎{member.mention}來到此伺服器！', color=self.successfulembedcolor)
                embed1.set_thumbnail(url=member.avatar_url)
                await channel.send(embed=embed1)
                if not jdata['logchannel'] == "未設定":
                    channel = self.bot.get_channel(
                        int(jdata['logchannel']))
                    if not channel == None:
                        logembed = discord.Embed(
                            title="有成員加入！", description="歡迎新成員！", color=self.successfulembedcolor)
                        logembed.add_field(
                            name="成員名稱", value=member.name, inline=False)
                        if member.status == discord.Status.online:
                            status = "上線"
                        elif member.status == discord.Status.idle:
                            status = "閒置"
                        elif member.status == discord.Status.dnd:
                            status = "勿擾"
                        elif member.status == discord.Status.offline:
                            status = "離線"
                        logembed.add_field(
                            name="狀態", value=status, inline=False)
                        logembed.add_field(name="加入時間", value=member.joined_at.strftime(
                            '%Y/%m/%d %p %I:%M:%S %Z'), inline=False)
                        logembed.add_field(name="帳戶創立時間", value=member.created_at.strftime(
                            '%Y/%m/%d %p %I:%M:%S %Z'), inline=False)
                        logembed.set_footer(
                            text=f"使用者ID：{member.id}", icon_url=member.avatar_url)
                        logembed.set_thumbnail(url=member.avatar_url)
                        await channel.send(embed=logembed)
            else:
                pass
        else:
            pass

    @commands.Cog.listener()
    async def on_member_remove(self, member):
        jdata = allserversetting.find_one({"_id": str(member.guild.id)})
        if not jdata == None:
            if not jdata['leavechannel'] == "未設定":
                channel = self.bot.get_channel(int(jdata['leavechannel']))
                embed2 = discord.Embed(
                    title='向成員說再見！', description=f'{member}離開了此伺服器，再見！{member}！', color=self.errorembedcolor)
                embed2.set_thumbnail(url=member.avatar_url)
                await channel.send(embed=embed2)
                if not jdata['logchannel'] == "未設定":
                    channel = self.bot.get_channel(
                        int(jdata['logchannel']))
                    if not channel == None:
                        logembed = discord.Embed(
                            title="有成員離開了！", description="向成員說再見！", color=self.errorembedcolor)
                        logembed.add_field(
                            name="成員名稱", value=member.name, inline=False)
                        logembed.add_field(name="加入時間", value=member.joined_at.strftime(
                            '%Y/%m/%d %p %I:%M:%S %Z'), inline=False)
                        logembed.add_field(name="帳戶創立時間", value=member.created_at.strftime(
                            '%Y/%m/%d %p %I:%M:%S %Z'), inline=False)
                        logembed.set_footer(
                            text=f"使用者ID：{member.id}", icon_url=member.avatar_url)
                        logembed.set_thumbnail(url=member.avatar_url)
                        await channel.send(embed=logembed)
            else:
                pass
        else:
            pass

    @commands.Cog.listener()
    async def on_member_update(self, before, after):
        if not(before.nick == after.nick):
            jdata = allserversetting.find_one({"_id": str(before.guild.id)})
            if not jdata == None:
                if not jdata['logchannel'] == "未設定":
                    channel = self.bot.get_channel(int(jdata['logchannel']))
                    embed1 = discord.Embed(
                        title="有成員更新了他的暱稱！", description=f"**修改前：**{before.nick}\n**修改後：**{after.nick}", color=self.normalembedcolor)
                    embed1.set_thumbnail(url=after.avatar_url)
                    embed1.set_footer(
                        text=f"{after.name}\n使用者ID：{after.id}", icon_url=after.avatar_url)
                    await channel.send(embed=embed1)
                else:
                    pass
            else:
                pass

    @commands.Cog.listener()
    async def on_bulk_message_delete(self, messages):
        jdata = allserversetting.find_one({"_id": str(messages[0].guild.id)})
        if not jdata == None:
            if not jdata['logchannel'] == "未設定":
                channel = self.bot.get_channel(
                    int(jdata['logchannel']))
                outmessage = ""
                for message in messages:
                    outmessage += f"[{message.author}]：{message.content} \n"
                embed1 = discord.Embed(
                    title=f"{len(messages)}則訊息在#{messages[0].channel}被刪除", description=outmessage, color=self.errorembedcolor)
                await channel.send(embed=embed1)
            else:
                pass
        else:
            pass

    @commands.Cog.listener()
    async def on_message_edit(self, before, after):
        jdata = allserversetting.find_one({"_id": str(before.guild.id)})
        if not jdata == None:
            if not jdata['logchannel'] == "未設定":
                userID = before.author.id
                channel = self.bot.get_channel(int(jdata['logchannel']))
                embed1 = discord.Embed(
                    title='訊息已編輯', description='有使用者編輯了他的訊息', color=self.normalembedcolor)
                embed1.add_field(
                    name='發訊息之使用者：', value=f"{before.author.mention}", inline=False)
                embed1.add_field(
                    name='於頻道：', value=f"<#{before.channel.id}>", inline=False)
                embed1.add_field(
                    name='修改前文字訊息：', value=f"{before.content}", inline=True)
                embed1.add_field(
                    name='修改後文字訊息：', value=f"{after.content}", inline=True)
                if not len(before.embeds) == 0:
                    embed1.add_field(name='修改前嵌入式訊息：',
                                     value=before.embeds, inline=False)
                if not len(after.embeds) == 0:
                    embed1.add_field(name='修改後嵌入式訊息：',
                                     value=after.embeds, inline=True)
                embed1.set_thumbnail(url=before.author.avatar_url)
                embed1.set_footer(text=f"{before.author}\n使用者ID：{before.author.id}",
                                  icon_url=before.author.avatar_url)
                await channel.send(embed=embed1)
            else:
                pass
        else:
            pass

    '''提及log'''
    @commands.Cog.listener()
    async def on_message(self, message):
        if not (len(message.mentions) == 0 and len(message.role_mentions) == 0):
            jdata = allserversetting.find_one({"_id": str(message.guild.id)})
            if not jdata == None:
                if not jdata['logchannel'] == "未設定":
                    channel = self.bot.get_channel(int(jdata['logchannel']))
                    rolemention = []
                    usermention = []
                    outrolemention = ""
                    outusermention = ""
                    if not len(message.mentions) == 0:
                        for m in message.mentions:
                            usermention.append(m.mention)
                        for a in range(0, len(usermention) - 1):
                            outusermention += f"{usermention[a]}、"
                        outusermention += usermention[-1]
                    if not len(message.role_mentions) == 0:
                        for r in message.role_mentions:
                            rolemention.append(r.mention)
                        for a in range(0, len(rolemention) - 1):
                            outrolemention += f"{rolemention[a]}、"
                        outrolemention += rolemention[-1]

                    embed1 = discord.Embed(
                        title="有人提及使用者！", color=self.normalembedcolor)
                    embed1.add_field(
                        name="使用者", value=f"{message.author.mention}", inline=True)
                    embed1.add_field(
                        name="頻道", value=f"{message.channel.mention}", inline=True)
                    embed1.add_field(
                        name="訊息連結", value=f"{message.jump_url}", inline=False)
                    if not len(message.mentions) == 0:
                        embed1.add_field(
                            name=f"提及之使用者[{len(usermention)}]：", value=outusermention, inline=False)
                    if not len(message.role_mentions) == 0:
                        embed1.add_field(
                            name=f"提及之身分組[{len(rolemention)}]：", value=outrolemention, inline=False)
                    embed1.set_thumbnail(url=message.author.avatar_url)
                    embed1.set_footer(
                        text=f"使用者ID：{message.author.id}", icon_url=message.author.avatar_url)
                    await channel.send(embed=embed1)
                else:
                    pass
            else:
                pass

    @commands.Cog.listener()
    async def on_message_delete(self, message):
        jdata = allserversetting.find_one({"_id": str(message.guild.id)})
        if not jdata == None:
            if not jdata['logchannel'] == "未設定":
                userID = message.author.id
                attachment = message.attachments
                channel = self.bot.get_channel(
                    int(jdata['logchannel']))
                embed1 = discord.Embed(
                    title='訊息已刪除', description='有使用者的訊息被刪除', color=self.errorembedcolor)
                embed1.add_field(
                    name='發訊息之使用者：', value=f'{message.author.mention}', inline=False)
                embed1.add_field(
                    name='於頻道：', value=f'<#{message.channel.id}>', inline=False)
                embed1.add_field(
                    name='訊息內容：', value=f'{message.content}', inline=False)
                if not len(message.attachments) == 0:
                    count = 1
                    for i in message.attachments:
                        embed1.add_field(
                            name=f"附件{count}", value=i.proxy_url)
                embed1.set_thumbnail(url=message.author.avatar_url)
                embed1.set_footer(text=message.author,
                                  icon_url=message.author.avatar_url)
                await channel.send(embed=embed1)
            else:
                pass
        else:
            pass

    @commands.Cog.listener()
    async def on_guild_channel_create(self, channel):
        jdata = allserversetting.find_one({"_id": str(channel.guild.id)})
        if not jdata == None:
            if not jdata['logchannel'] == "未設定":
                if type(channel) == discord.channel.TextChannel:
                    outputchannel = self.bot.get_channel(
                        int(jdata['logchannel']))
                    createchannelname = channel.name
                    createchannelid = channel.id
                    createchanneltopic = channel.topic
                    createchannelposition = channel.position
                    messagedelay = channel.slowmode_delay
                    isnsfw = channel.is_nsfw()
                    isnews = channel.is_news()
                    undercategory = channel.category.name
                    embed1 = discord.Embed(
                        title="文字頻道已建立", description="新文字頻道已被建立", color=self.normalembedcolor)
                    embed1.add_field(
                        name="頻道名稱", value=createchannelname, inline=True)
                    embed1.add_field(
                        name="頻道主題", value=createchanneltopic, inline=True)
                    embed1.add_field(
                        name="所屬類別", value=undercategory, inline=False)
                    embed1.add_field(
                        name="頻道編號", value=createchannelposition, inline=False)
                    embed1.add_field(
                        name="是否為18+頻道", value=isnsfw, inline=True)
                    embed1.add_field(name="是否為新聞頻道", value=isnews, inline=True)
                    embed1.add_field(
                        name="延遲秒數", value=messagedelay, inline=True)
                    embed1.set_footer(text=f"頻道ID：{createchannelid}")
                    await outputchannel.send(embed=embed1)
                elif type(channel) == discord.channel.VoiceChannel:
                    outputchannel = self.bot.get_channel(
                        int(jdata['logchannel']))
                    createchannelname = channel.name
                    createchannelid = channel.id
                    createchannelposition = channel.position
                    undercategory = channel.category.name
                    embed1 = discord.Embed(
                        title="語音頻道已建立", description="新語音頻道已建立", color=self.normalembedcolor)
                    embed1.add_field(
                        name="頻道名稱", value=createchannelname, inline=True)
                    embed1.add_field(
                        name="所屬類別", value=undercategory, inline=False)
                    embed1.add_field(
                        name="頻道編號", value=createchannelposition, inline=True)
                    embed1.set_footer(text=f"頻道ID:{createchannelid}")
                    await outputchannel.send(embed=embed1)
            else:
                pass
        else:
            pass

    @commands.Cog.listener()
    async def on_guild_channel_delete(self, channel):
        jdata = allserversetting.find_one({"_id": str(channel.guild.id)})
        if not jdata == None:
            if not jdata['logchannel'] == "未設定":
                if type(channel) == discord.channel.TextChannel:
                    outputchannel = self.bot.get_channel(
                        int(jdata['logchannel']))
                    createchannelname = channel.name
                    createchannelid = channel.id
                    createchanneltopic = channel.topic
                    createchannelposition = channel.position
                    messagedelay = channel.slowmode_delay
                    isnsfw = channel.is_nsfw()
                    isnews = channel.is_news()
                    undercategory = channel.category.name
                    embed1 = discord.Embed(
                        title="文字頻道已刪除", description=f"文字頻道已被刪除", color=self.errorembedcolor)
                    embed1.add_field(
                        name="頻道名稱", value=createchannelname, inline=True)
                    embed1.add_field(
                        name="頻道主題", value=createchanneltopic, inline=True)
                    embed1.add_field(
                        name="所屬類別", value=undercategory, inline=False)
                    embed1.add_field(
                        name="頻道編號", value=createchannelposition, inline=False)
                    embed1.add_field(
                        name="是否為18+頻道", value=isnsfw, inline=True)
                    embed1.add_field(name="是否為新聞頻道", value=isnews, inline=True)
                    embed1.add_field(
                        name="延遲秒數", value=messagedelay, inline=True)
                    embed1.set_footer(text=f"頻道ID：{createchannelid}")
                    await outputchannel.send(embed=embed1)
                elif type(channel) == discord.channel.VoiceChannel:
                    outputchannel = self.bot.get_channel(
                        int(jdata['logchannel']))
                    createchannelname = channel.name
                    createchannelid = channel.id
                    createchannelposition = channel.position
                    undercategory = channel.category.name
                    embed1 = discord.Embed(
                        title="語音頻道已刪除", description="語音頻道已被刪除", color=self.errorembedcolor)
                    embed1.add_field(
                        name="頻道名稱", value=createchannelname, inline=True)
                    embed1.add_field(
                        name="所屬類別", value=undercategory, inline=False)
                    embed1.add_field(
                        name="頻道編號", value=createchannelposition, inline=True)
                    embed1.set_footer(text=f"頻道ID:{createchannelid}")
                    await outputchannel.send(embed=embed1)
            else:
                pass
        else:
            pass

    @commands.Cog.listener()
    async def on_user_update(self, before, after):
        outputchannelid = []
        for server in self.bot.guilds:
            if before in server.members:
                outputchannelid.append(int(server.id))
        if before.avatar != after.avatar:
            for outputchannelids in outputchannelid:
                jdata = allserversetting.find_one(
                    {"_id": str(outputchannelids)})
                if not jdata == None:
                    if not jdata['logchannel'] == "未設定":
                        outputchannel = self.bot.get_channel(
                            int(jdata['logchannel']))
                        embed1 = discord.Embed(
                            title="有使用者更新了他的頭像！", description=f"{after.mention}", color=self.normalembedcolor)
                        embed1.set_thumbnail(url=after.avatar_url)
                        embed1.set_footer(
                            text=after.name, icon_url=after.avatar_url)
                        await outputchannel.send(embed=embed1)
                    else:
                        pass
                else:
                    pass
        elif before.name != after.name:
            for outputchannelids in outputchannelid:
                jdata = allserversetting.find_one(
                    {"_id": str(outputchannelids)})
                if not jdata == None:
                    if not(jdata['logchannel'] == "未設定"):
                        outputchannel = self.bot.get_channel(
                            int(jdata['logchannel']))
                        embed1 = discord.Embed(
                            title="有使用者更新了他的名稱！", description=f"**修改前：**{before.name}\n**修改後：**{after.name}", color=self.normalembedcolor)
                        embed1.set_thumbnail(url=after.avatar_url)
                        embed1.set_footer(
                            text=after.name, icon_url=after.avatar_url)
                        await outputchannel.send(embed=embed1)
                    else:
                        pass
                else:
                    pass
        elif before.discriminator != after.discriminator:
            for outputchannelids in outputchannelid:
                jdata = allserversetting.find_one(
                    {"_id": str(outputchannelids)})
                if not jdata == None:
                    if not(jdata['logchannel'] == "未設定"):
                        outputchannel = self.bot.get_channel(
                            int(jdata['logchannel']))
                        embed1 = discord.Embed(
                            title="有使用者更新了他的編號！", description=f"**修改前：**{before.discriminator}\n**修改後：**{after.discriminator}", color=self.normalembedcolor)
                        embed1.set_thumbnail(url=after.avatar_url)
                        embed1.set_footer(
                            text=after.name, icon_url=after.avatar_url)
                        await outputchannel.send(embed=embed1)
                    else:
                        pass
                else:
                    pass


def setup(bot):
    bot.add_cog(Logging(bot))
