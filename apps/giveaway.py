import discord
import dns
import pymongo
import datetime
import json
from discord.ext import commands
from core.classes import *
from typing import Union

with open("JSON/setting.json", mode="r", encoding="utf8")as jfile:
    jdata = json.load(jfile)

client = pymongo.MongoClient(
    jdata['MongoDBWebsite']
)
giveawayall = client['KayBotPython']['giveaway']


class Giveaway(Cog_Extension):
    @commands.command()
    async def creategiveaway(self, ctx, inchannel: Union[discord.TextChannel, int], inendtime: str, canget: int, inreward: str):
        channel = inchannel
        endtime = inendtime
        reward = inreward
        thegiveaway = {}
        if isinstance(channel, int):
            channel = discord.utils.get(ctx.guild.channels, id=channel)
            if channel == None:
                await ctx.channel.send("請指定一個存在的頻道！")
                return
        embed1 = discord.Embed(
            title="新抽獎！", description=f":gift: 獎勵：{reward}\n :clock12: 截止時間：{endtime}")
        message = await channel.send(embed=embed1)
        thegiveaway['_id'] = str(channel.id)+str(message.id)+"🎉"
        thegiveaway['channel'] = channel.id
        thegiveaway['message'] = message.id
        try:
            iftime = datetime.datetime.strptime(endtime, "")
        except ValueError:
            await ctx.channel.send("請輸入正確時間格式！")
            return

    @creategiveaway.error
    async def creategiveawayerror(self, ctx, error):
        await ctx.channel.send("還沒完工唷~~ :blue_heart:")

    @commands.Cog.listener()
    async def on_raw_reaction_add(self, payload):
        if payload.emoji == "🎉":
            thegiveawaydata = giveawayall.find_one(
                {"_id": str(payload.channel_id)+str(payload.message_id)+"🎉"})
            if not thegiveawaydata == None:
                if thegiveawaydata['end'] == False:
                    thegiveawaydata['people'].append(payload.user_id)
                    giveawayall.update_one({"_id": str(payload.channel_id)+str(
                        payload.message_id)+"🎉"}, {"$set": {"people": thegiveawaydata['people']}})

    @commands.Cog.listener()
    async def on_raw_reaction_remove(self, payload):
        if payload.emoji == "🎉":
            thegiveawaydata = giveawayall.find_one(
                {"_id": str(payload.channel_id)+str(payload.message_id)+"🎉"})
            if not thegiveawaydata == None:
                if thegiveawaydata['end'] == False:
                    thelist = thegiveawaydata['people']
                    index = thelist.index(payload.user_id)
                    del thelist[index]
                    giveawayall.update_one({"_id": str(payload.channel_id)+str(
                        payload.message_id)+"🎉"}, {"$set": {"people": thegiveawaydata['people']}})


def setup(bot):
    bot.add_cog(Giveaway(bot))
