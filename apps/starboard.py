import discord
import json
from discord.ext import commands
from core.classes import *

with open('JSON/serversettings.json', mode='r', encoding='utf8')as jfile:
    serversettings = json.load(jfile)

with open('JSON/starboard.json', mode='r', encoding='utf8') as jfile:
    starboard = json.load(jfile)


class StarBoard(Cog_Extension):
    @commands.command()
    async def setpoststar(self, ctx, needstar: str):
        await ctx.channel.send("還沒完工唷~~ :blue_heart:")

    @setpoststar.error
    async def setpoststarerror(self, ctx, error):
        await ctx.channel.send("還沒完工唷~~ :blue_heart:")

    @commands.Cog.listener()
    async def on_reaction_add(self, reaction, user):
        if(str(reaction.emoji) == "⭐"):
            if not(reaction.message.author.bot):
                try:
                    if str(reaction.message.id) in starboard[str(reaction.message.guild.id)]:
                        starreaction = 0
                        for allreactions in reaction.message.reactions:
                            if str(allreactions.emoji) == "⭐":
                                starreaction += 1
                        guildid = str(reaction.message.guild.id)
                        messageid = str(reaction.message.id)
                        starboard[guildid][messageid]['starcount'] = starreaction
                        with open("JSON/starboard.json", mode="w", encoding="utf8") as finish:
                            json.dump(starboard, finish, sort_keys=True,
                                      indent=4, ensure_ascii=True)
                    else:
                        starreaction = 0
                        for allreactions in reaction.message.reactions:
                            if str(allreactions.emoji) == "⭐":
                                starreaction += 1
                        if starreaction >= int(serversettings[str(reaction.message.guild.id)]['poststar']):
                            guildid = str(reaction.message.guild.id)
                            messageid = str(reaction.message.id)
                            message = reaction.message
                            if guildid not in serversettings:
                                starboard[guildid] = {}
                            starboard[guildid][messageid] = {}
                            starboard[guildid][messageid]['messageid'] = int(
                                messageid)
                            starboard[guildid][messageid]['channelid'] = int(
                                reaction.message.channel)
                            starboard[guildid][messageid]['starcount'] = starreaction
                            with open("JSON/starboard.json", mode="w", encoding="utf8") as finish:
                                json.dump(starboard, finish, sort_keys=True,
                                          indent=4, ensure_ascii=True)
                except KeyError:
                    await reaction.channel.send("程式有錯")


def setup(bot):
    bot.add_cog(StarBoard(bot))
