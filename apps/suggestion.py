import discord
from discord.ext import commands
from core.classes import *
import json

with open("JSON/serversuggestion.json", mode="r", encoding="utf8") as jfile:
    serversuggestion = json.load(jfile)

with open("JSON/botsuggestion.json", mode="r", encoding="utf8") as jfile:
    botsuggestion = json.load(jfile)

with open("JSON/serversettings.json", mode="r", encoding="utf8") as jfile:
    serversettings = json.load(jfile)

with open("JSON/count.json", mode="r", encoding="utf8") as jfile:
    counts = json.load(jfile)


class Suggestion(Cog_Extension):
    @commands.command()
    async def makesuggestion(self, ctx, types: str, *, suggestion: str):
        serverid = str(ctx.guild.id)
        if types == "bot":
            channel = self.bot.get_channel(681100880346742825)
            counts['botsuggestioncount'] += 1
            botsuggestioncount = str(counts['botsuggestioncount'])
            botsuggestion[botsuggestioncount] = {}
            embed1 = discord.Embed(title=f"機器人建議#{counts['botsuggestioncount']}",
                                   description=f"**來自伺服器：{ctx.guild.name}**\n**建議內容：{suggestion}**", color=self.normalembedcolor)
            botsuggestion[botsuggestioncount]['suggestion'] = suggestion
            botsuggestion[botsuggestioncount]['author'] = ctx.author.id
            botsuggestion[botsuggestioncount]['guild'] = ctx.guild.id
            botsuggestion[botsuggestioncount]['status'] = 'waiting'
            messageid = await channel.send(embed=embed1)
            botsuggestion[botsuggestioncount]['messageid'] = messageid.id
            await messageid.add_reaction("✅")
            await messageid.add_reaction("❌")
            with open("JSON/botsuggestion.json", mode='w', encoding='utf8')as finish:
                json.dump(botsuggestion, finish, sort_keys=True,
                          indent=4, ensure_ascii=False)
            with open("JSON/count.json", mode='w', encoding='utf8')as finish:
                json.dump(counts, finish, sort_keys=True,
                          indent=4, ensure_ascii=False)
            await ctx.channel.send("機器人建議已成功發送！")
        elif types == "server":
            channel = self.bot.get_channel(
                serversettings[serverid]['suggestionchannel'])
            if serverid not in counts:
                counts[serverid] = {}
                counts[serverid]['suggestioncount'] = 0
            if serverid not in serversuggestion:
                serversuggestion[serverid] = {}
            counts[serverid]['suggestioncount'] += 1
            suggestioncount = str(counts[serverid]['suggestioncount'])
            serversuggestion[serverid][suggestioncount] = {}
            serversuggestion[serverid][suggestioncount]['suggestion'] = suggestion
            serversuggestion[serverid][suggestioncount]['author'] = ctx.author.id
            serversuggestion[serverid][suggestioncount]['status'] = 'waiting'
            embed1 = discord.Embed(
                title=f'伺服器建議#{suggestioncount}', description=suggestion, color=self.normalembedcolor)
            messageid = await channel.send(embed=embed1)
            serversuggestion[serverid][suggestioncount]['messageid'] = messageid.id
            await messageid.add_reaction("✅")
            await messageid.add_reaction("❌")
            with open("JSON/serversuggestion.json", mode='w', encoding='utf8')as finish:
                json.dump(serversuggestion, finish, sort_keys=True,
                          indent=4, ensure_ascii=False)
            with open("JSON/count.json", mode='w', encoding='utf8')as finish:
                json.dump(counts, finish, sort_keys=True,
                          indent=4, ensure_ascii=False)
            await ctx.channel.send("伺服器建議已成功發送！")
        else:
            embed1 = discord.Embed(title='請指定要給的建議之類型！',
                                   description='不會用嗎？沒關係，我幫你', color=self.normalembedcolor)
            embed1.add_field(
                name='用法', value='``s!suggestion [類型] [內容]``', inline=True)
            embed1.add_field(
                name='範例', value='``s!suggestion bot 新增音樂功能``', inline=True)
            embed1.add_field(
                name='類型選項', value='**bot**：給予此bot開發建議\n**server**：給予該伺服器任何建議', inline=False)
            await ctx.channel.send(embed=embed1)

    @makesuggestion.error
    async def makesuggestion_error(self, ctx, error):
        if isinstance(error, discord.ext.commands.MissingRequiredArgument):
            embed1 = discord.Embed(
                title='請輸入要給之建議的類型與內容！', description='不會用嗎？沒關係，我幫你', color=self.normalembedcolor)
            embed1.add_field(
                name='用法', value='``s!suggestion [類型] [內容]``', inline=True)
            embed1.add_field(
                name='範例', value='``s!suggestion bot 新增音樂功能``', inline=True)
            embed1.add_field(
                name='類型選項', value='**bot**：給予此bot開發建議\n**server**：給予該伺服器任何建議', inline=False)
            await ctx.channel.send(embed=embed1)
        else:
            embed1 = discord.Embed(
                title='輸出失敗！', description='對不起，無法執行！', color=self.errorembedcolor)
            embed1.add_field(name='錯誤訊息：', value=error)
            await ctx.channel.send(embed=embed1)

    @commands.command()
    @commands.has_permissions(manage_guild=True)
    async def approve(self, ctx, types: str, num: int, *, reason: str):
        try:
            if not (serversettings[str(ctx.guild.id)]['suggestionchannel'] == "未設定"):
                try:
                    if types == "bot":
                        channel = self.bot.get_channel(681100880346742825)
                        message = await channel.fetch_message(botsuggestion[str(num)]['messageid'])
                        sentguild = await self.bot.fetch_guild(int(botsuggestion[str(num)]['guild']))
                        sentguildname = sentguild.name
                        suggestioncontent = str(
                            botsuggestion[str(num)]['suggestion'])
                        embed1 = discord.Embed(
                            title=f'建議#{num}', description=f"建議內容：{suggestioncontent}\n來自伺服器：{sentguildname}", color=self.successfulembedcolor)
                        embed1.add_field(name="原因", value=reason)
                        embed1.set_footer(
                            text=ctx.author, icon_url=ctx.author.avatar_url)
                        botsuggestion[str(num)]['status'] = "approve"
                        with open("JSON/botsuggestion.json", mode="w", encoding='utf8') as finish:
                            json.dump(botsuggestion, finish, sort_keys=True,
                                      indent=4, ensure_ascii=False)
                        await message.edit(content="建議已批准！", embed=embed1)
                        await ctx.channel.send("批改建議成功！")
                        makesuggestioner = await self.bot.fetch_user(int(botsuggestion[str(num)]['author']))
                        await makesuggestioner.send(content=f"恭喜！您的機器人建議已被批准！原因為：{reason}", embed=embed1)
                        await ctx.channel.send("私訊發送成功！")
                    elif types == "server":
                        channel = self.bot.get_channel(
                            int(serversettings[str(ctx.guild.id)]['suggestionchannel']))
                        message = await channel.fetch_message(int(serversuggestion[str(ctx.guild.id)][str(num)]['messageid']))
                        suggestioncontent = str(
                            serversuggestion[str(ctx.guild.id)][str(num)]['suggestion'])
                        embed1 = discord.Embed(
                            title=f"建議#{num}", description=f"{suggestioncontent}", color=self.successfulembedcolor)
                        embed1.add_field(name="原因", value=reason)
                        embed1.set_footer(
                            text=ctx.author, icon_url=ctx.author.avatar_url)
                        serversuggestion[str(ctx.guild.id)][str(
                            num)]['status'] = "approve"
                        with open("JSON/serversuggestion.json", mode="w", encoding="utf8") as finish:
                            json.dump(serversuggestion, finish,
                                      sort_keys=True, indent=4, ensure_ascii=False)
                        await message.edit(content="建議已批准", embed=embed1)
                        await ctx.channel.send("批改建議成功！")
                        makesuggestioner = await self.bot.fetch_user(int(serversuggestion[str(ctx.guild.id)][str(num)]['author']))
                        await makesuggestioner.send(content=f"恭喜！您的伺服器建議已被批准！原因為：{reason}", embed=embed1)
                        await ctx.channel.send("私訊發送成功！")
                except KeyError:
                    await ctx.channel.send("查無此建議")
            else:
                await ctx.channel.send("您尚未設定建議頻道！")
        except KeyError:
            await ctx.channel.send("請先產生空白設定檔！")

    @commands.command()
    @commands.has_permissions(manage_guild=True)
    async def implemented(self, ctx, types: str, num: int, *, reason: str):
        try:
            if not (serversettings[str(ctx.guild.id)]['suggestionchannel'] == "未設定"):
                try:
                    if types == "bot":
                        channel = self.bot.get_channel(681100880346742825)
                        message = await channel.fetch_message(botsuggestion[str(num)]['messageid'])
                        sentguild = await self.bot.fetch_guild(int(botsuggestion[str(num)]['guild']))
                        sentguildname = sentguild.name
                        suggestioncontent = str(
                            botsuggestion[str(num)]['suggestion'])
                        embed1 = discord.Embed(
                            title=f'建議#{num}', description=f"建議內容：{suggestioncontent}\n來自伺服器：{sentguildname}", color=0x91fbff)
                        embed1.add_field(name="原因", value=reason)
                        embed1.set_footer(
                            text=ctx.author, icon_url=ctx.author.avatar_url)
                        botsuggestion[str(num)]['status'] = "implemented"
                        with open("JSON/botsuggestion.json", mode="w", encoding='utf8') as finish:
                            json.dump(botsuggestion, finish, sort_keys=True,
                                      indent=4, ensure_ascii=False)
                        await message.edit(content="建議已實施", embed=embed1)
                        await ctx.channel.send("批改建議成功！")
                        makesuggestioner = await self.bot.fetch_user(int(botsuggestion[str(num)]['author']))
                        await makesuggestioner.send(content=f"恭喜！您的機器人建議已實施！原因為：{reason}", embed=embed1)
                        await ctx.channel.send("私訊發送成功！")
                    elif types == "server":
                        channel = self.bot.get_channel(
                            int(serversettings[str(ctx.guild.id)]['suggestionchannel']))
                        message = await channel.fetch_message(int(serversuggestion[str(ctx.guild.id)][str(num)]['messageid']))
                        suggestioncontent = str(
                            serversuggestion[str(ctx.guild.id)][str(num)]['suggestion'])
                        embed1 = discord.Embed(
                            title=f"建議#{num}", description=f"{suggestioncontent}", color=0x91fbff)
                        embed1.add_field(name="原因", value=reason)
                        embed1.set_footer(
                            text=ctx.author, icon_url=ctx.author.avatar_url)
                        serversuggestion[str(ctx.guild.id)][str(
                            num)]['status'] = "implemented"
                        with open("JSON/serversuggestion.json", mode="w", encoding="utf8") as finish:
                            json.dump(serversuggestion, finish,
                                      sort_keys=True, indent=4, ensure_ascii=False)
                        await message.edit(content="建議已實施", embed=embed1)
                        await ctx.channel.send("批改建議成功！")
                        makesuggestioner = await self.bot.fetch_user(int(serversuggestion[str(ctx.guild.id)][str(num)]['author']))
                        await makesuggestioner.send(content=f"恭喜！您的伺服器建議已實施！原因為：{reason}", embed=embed1)
                        await ctx.channel.send("私訊發送成功！")
                except KeyError:
                    await ctx.channel.send("查無此建議")
            else:
                await ctx.channel.send("您尚未設定建議頻道！")
        except KeyError:
            await ctx.channel.send("請先產生空白設定檔！")

    @commands.command()
    @commands.has_permissions(manage_guild=True)
    async def consider(self, ctx, types: str, num: int, *, reason: str):
        try:
            if not (serversettings[str(ctx.guild.id)]['suggestionchannel'] == "未設定"):
                try:
                    if types == "bot":
                        channel = self.bot.get_channel(681100880346742825)
                        message = await channel.fetch_message(botsuggestion[str(num)]['messageid'])
                        sentguild = await self.bot.fetch_guild(int(botsuggestion[str(num)]['guild']))
                        sentguildname = sentguild.name
                        suggestioncontent = str(
                            botsuggestion[str(num)]['suggestion'])
                        embed1 = discord.Embed(
                            title=f'建議#{num}', description=f"建議內容：{suggestioncontent}\n來自伺服器：{sentguildname}", color=0xfff987)
                        embed1.add_field(name="原因", value=reason)
                        embed1.set_footer(
                            text=ctx.author, icon_url=ctx.author.avatar_url)
                        botsuggestion[str(num)]['status'] = "consider"
                        with open("JSON/botsuggestion.json", mode="w", encoding='utf8') as finish:
                            json.dump(botsuggestion, finish, sort_keys=True,
                                      indent=4, ensure_ascii=False)
                        await message.edit(content="建議正在考量中", embed=embed1)
                        await ctx.channel.send("批改建議成功！")
                        makesuggestioner = await self.bot.fetch_user(int(botsuggestion[str(num)]['author']))
                        await makesuggestioner.send(content=f"您的機器人建議管理員正在思考中！原因為：{reason}", embed=embed1)
                        await ctx.channel.send("私訊發送成功！")
                    elif types == "server":
                        channel = self.bot.get_channel(
                            int(serversettings[str(ctx.guild.id)]['suggestionchannel']))
                        message = await channel.fetch_message(int(serversuggestion[str(ctx.guild.id)][str(num)]['messageid']))
                        suggestioncontent = str(
                            serversuggestion[str(ctx.guild.id)][str(num)]['suggestion'])
                        embed1 = discord.Embed(
                            title=f"建議#{num}", description=f"{suggestioncontent}", color=0xfff987)
                        embed1.add_field(name="原因", value=reason)
                        embed1.set_footer(
                            text=ctx.author, icon_url=ctx.author.avatar_url)
                        serversuggestion[str(ctx.guild.id)][str(
                            num)]['status'] = "consider"
                        with open("JSON/serversuggestion.json", mode="w", encoding="utf8") as finish:
                            json.dump(serversuggestion, finish,
                                      sort_keys=True, indent=4, ensure_ascii=False)
                        await message.edit(content="建議正在考量中", embed=embed1)
                        await ctx.channel.send("批改建議成功！")
                        makesuggestioner = await self.bot.fetch_user(int(serversuggestion[str(ctx.guild.id)][str(num)]['author']))
                        await makesuggestioner.send(content=f"您的伺服器建議管理員正在思考中！原因為：{reason}", embed=embed1)
                        await ctx.channel.send("私訊發送成功！")
                except KeyError:
                    await ctx.channel.send("查無此建議")
            else:
                await ctx.channel.send("您尚未設定建議頻道！")
        except KeyError:
            await ctx.channel.send("請先產生空白設定檔！")

    @commands.command()
    @commands.has_permissions(manage_guild=True)
    async def deny(self, ctx, types: str, num: int, *, reason: str):
        try:
            if not (serversettings[str(ctx.guild.id)]['suggestionchannel'] == "未設定"):
                try:
                    if types == "bot":
                        channel = self.bot.get_channel(681100880346742825)
                        message = await channel.fetch_message(botsuggestion[str(num)]['messageid'])
                        sentguild = await self.bot.fetch_guild(int(botsuggestion[str(num)]['guild']))
                        sentguildname = sentguild.name
                        suggestioncontent = str(
                            botsuggestion[str(num)]['suggestion'])
                        embed1 = discord.Embed(
                            title=f'建議#{num}', description=f"建議內容：{suggestioncontent}\n來自伺服器：{sentguildname}", color=self.errorembedclor)
                        embed1.add_field(name="原因", value=reason)
                        embed1.set_footer(
                            text=ctx.author, icon_url=ctx.author.avatar_url)
                        botsuggestion[str(num)]['status'] = "deny"
                        with open("JSON/botsuggestion.json", mode="w", encoding='utf8') as finish:
                            json.dump(botsuggestion, finish, sort_keys=True,
                                      indent=4, ensure_ascii=False)
                        await message.edit(content="建議已拒絕", embed=embed1)
                        await ctx.channel.send("批改建議成功！")
                        makesuggestioner = await self.bot.fetch_user(int(botsuggestion[str(num)]['author']))
                        await makesuggestioner.send(content=f"很不幸地，你的機器人建議被管理員拒絕！原因為：{reason}", embed=embed1)
                        await ctx.channel.send("私訊發送成功！")
                    elif types == "server":
                        channel = self.bot.get_channel(
                            int(serversettings[str(ctx.guild.id)]['suggestionchannel']))
                        message = await channel.fetch_message(int(serversuggestion[str(ctx.guild.id)][str(num)]['messageid']))
                        suggestioncontent = str(
                            serversuggestion[str(ctx.guild.id)][str(num)]['suggestion'])
                        embed1 = discord.Embed(
                            title=f"建議#{num}", description=f"{suggestioncontent}", color=self.errorembedclor)
                        embed1.add_field(name="原因", value=reason)
                        embed1.set_footer(
                            text=ctx.author, icon_url=ctx.author.avatar_url)
                        serversuggestion[str(ctx.guild.id)][str(
                            num)]['status'] = "deny"
                        with open("JSON/serversuggestion.json", mode="w", encoding="utf8") as finish:
                            json.dump(serversuggestion, finish,
                                      sort_keys=True, indent=4, ensure_ascii=False)
                        await message.edit(content="建議已拒絕", embed=embed1)
                        await ctx.channel.send("批改建議成功！")
                        makesuggestioner = await self.bot.fetch_user(int(serversuggestion[str(ctx.guild.id)][str(num)]['author']))
                        await makesuggestioner.send(content=f"很不幸地，你的伺服器建議被管理員拒絕！原因為：{reason}", embed=embed1)
                        await ctx.channel.send("私訊發送成功")
                except KeyError:
                    await ctx.channel.send("查無此建議")
            else:
                await ctx.channel.send("您尚未設定建議頻道！")
        except KeyError:
            await ctx.channel.send("請先產生空白設定檔！")


def setup(bot):
    bot.add_cog(Suggestion(bot))
