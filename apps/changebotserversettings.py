import discord
import pymongo
import dns
from discord.ext import commands
from core.classes import *
from typing import Union
import json
import os

with open("JSON/setting.json", mode="r", encoding="utf8")as jfile:
    jdata = json.load(jfile)

client = pymongo.MongoClient(
    jdata['MongoDBWebsite']
)
allserversetting = client['KayBotPython']['serversetting']


class ChangeBotServerSettings(Cog_Extension):
    # 指令
    @commands.command()
    @commands.has_permissions(manage_guild=True)
    async def showsettings(self, ctx):
        serversettings = allserversetting.find_one({"_id": str(ctx.guild.id)})
        if not serversettings == None:
            outputmsg = ""
            outputmsg += f"**歡迎頻道：**<#{serversettings['welcomechannel']}>\n"
            outputmsg += f"**離開頻道：**<#{serversettings['leavechannel']}>\n"
            outputmsg += f"**挑戰公布頻道：**<#{serversettings['challengechannel']}>\n"
            outputmsg += f"**挑戰提及身分組：**<#{serversettings['challengemention']}>\n"
            outputmsg += f"**建議頻道：**<#{serversettings['suggestionchannel']}>\n"
            outputmsg += f"**和機器人聊天頻道：**<#{serversettings['funchat']}>\n"
            outputmsg += f"**記錄頻道：**<#{serversettings['logchannel']}>\n"
            outputmsg += f"**公告頻道：**<#{serversettings['announcechannel']}>\n"
            outputmsg += f"**訊息排行榜頻道**<#{serversettings['starboardchannel']}>"
            outputmsg += f"**轉貼至訊息排行榜之星數：**{serversettings['poststar']}"
            embed1 = discord.Embed(
                title="此機器人在此伺服器之設定",
                description=outputmsg,
                color=self.normalembedcolor)
            await ctx.channel.send(embed=embed1)
        else:
            await ctx.channel.send("您尚未為您的伺服器產生空白設定")

    @commands.command()
    @commands.has_permissions(manage_guild=True)
    async def makesettings(self, ctx):
        serversettings = allserversetting.find_one({"_id": str(ctx.guild.id)})
        if serversettings == None:
            toinsert = {}
            toinsert['welcomechannel'] = "未設定"
            toinsert['leavechannel'] = "未設定"
            toinsert['challengechannel'] = "未設定"
            toinsert['challengemention'] = "未設定"
            toinsert['suggestionchannel'] = "未設定"
            toinsert['funchat'] = "未設定"
            toinsert['logchannel'] = "未設定"
            toinsert['gamequestion'] = False
            toinsert['normalquestion'] = False
            toinsert['announcechannel'] = "未設定"
            toinsert['starboardchannel'] = "未設定"
            toinsert['poststar'] = 0
            allserversetting.insert_one(toinsert)
            await ctx.channel.send("空白設定已建立完成！")
        else:
            await ctx.channel.send("您已經為您的伺服器產生空白設定檔")

    @commands.command()
    @commands.has_permissions(manage_guild=True)
    async def setchannel(self, ctx, types: str, channel: discord.TextChannel):
        serversettings = allserversetting.find_one({"_id": str(ctx.guild.id)})
        if not serversettings == None:
            if types == 'suggestionchannel':
                serversettings['suggestionchannel'] = channel.id
                allserversetting.update_one({
                    "_id": str(ctx.guild.id)
                }, {
                    "$set": {
                        "suggestionchannel":
                        serversettings['suggestionchannel']
                    }
                })
                await ctx.channel.send("更改完畢")
            elif types == 'logchannel':
                serversettings['logchannel'] = channel.id
                allserversetting.update_one({
                    "_id": str(ctx.guild.id)
                }, {"$set": {
                    "logchannel": serversettings['logchannel']
                }})
                await ctx.channel.send("更改完畢")
            elif types == 'welcomechannel':
                serversettings['welcomechannel'] = channel.id
                allserversetting.update_one({
                    "_id": str(ctx.guild.id)
                }, {
                    "$set": {
                        "welcomechannel": serversettings['welcomechannel']
                    }
                })
                await ctx.channel.send("更改完畢")
            elif types == 'challengechannel':
                serversettings['challengechannel'] = channel.id
                allserversetting.update_one({
                    "_id": str(ctx.guild.id)
                }, {
                    "$set": {
                        "challengechannel": serversettings['challengechannel']
                    }
                })
                await ctx.channel.send("更改完畢")
            elif types == 'leavechannel':
                serversettings['leavechannel'] = channel.id
                allserversetting.update_one({
                    "_id": str(ctx.guild.id)
                }, {"$set": {
                    "leavechannel": serversettings['leavechannel']
                }})
                await ctx.channel.send("更改完畢")
            elif types == 'funchat':
                serversettings['funchat'] = channel.id
                allserversetting.update_one({
                    "_id": str(ctx.guild.id)
                }, {"$set": {
                    "funchat": serversettings['funchat']
                }})
                await ctx.channel.send("更改完畢")
            elif types == 'announcechannel':
                serversettings['announcechannel'] = channel.id
                allserversetting.update_one({
                    "_id": str(ctx.guild.id)
                }, {
                    "$set": {
                        "announcechannel": serversettings['announcechannel']
                    }
                })
                await ctx.channel.send("更改完畢")
            elif types == 'starboardchannel':
                serversettings['starboardchannel'] = channel.id
                allserversetting.update_one({
                    "_id": str(ctx.guild.id)
                }, {
                    "$set": {
                        "starboardchannel": serversettings['starboardchannel']
                    }
                })
                await ctx.channel.send("更改完畢")
            elif types == 'levelmessagechannel':
                serversettings['levelmessagechannel'] = channel.id
                allserversetting.update_one({
                    "_id": str(ctx.guild.id)
                }, {
                    "$set": {
                        "levelmessagechannel": serversettings['levelmessagechannel']
                    }
                })
                await ctx.channel.send("更改完畢")
        else:
            await ctx.channel.send("請先建立空白設定檔！")

    @setchannel.error
    async def set_channel_error(self, ctx, error):
        if isinstance(error, discord.ext.commands.CheckFailure):
            embed1 = discord.Embed(
                title='權限不足！',
                description='您沒有權限執行此指令！',
                color=self.errorembedcolor)
            embed1.add_field(name='請確認您是否有以下權限：', value='管理伺服器')
            await ctx.channel.send(embed=embed1)
        elif isinstance(error, discord.ext.commands.MissingRequiredArgument):
            embed1 = discord.Embed(
                title='請輸入設定類型與頻道！',
                description='不會用嗎？沒關係，我幫你',
                color=self.normalembedcolor)
            embed1.add_field(
                name='用法usage', value='``s!setchannel [類型] [提及頻道]``', inline=False)
            embed1.add_field(
                name='範例example', value='``s!setchannel welcomechannel #主要聊天``', inline=False)
            embed1.add_field(
                name='類型選項', value="**suggestionchannel**:建議傳送頻道\n**logchannel**:日誌傳送頻道\n**welcomechannel**:歡迎訊息傳送頻道\n**leavechannel**:離開訊息傳送頻道\n**challengechannel**:挑戰訊息傳送頻道", inline=False)
            await ctx.channel.send(embed=embed1)
        else:
            embed1 = discord.Embed(
                title='清除失敗！',
                description='對不起，無法設定！若問題持續發生，請加入支援伺服器或著連絡美味的小圓#0726',
                color=self.errorembedcolor)
            embed1.add_field(name='錯誤訊息：', value=error)
            await ctx.channel.send(embed=embed1)

    @commands.command()
    @commands.has_permissions(manage_guild=True)
    async def setmention(self, ctx, types: str, setrole: discord.Role):
        serversettings = allserversetting.find_one({"_id": str(ctx.guild.id)})
        if not serversettings == None:
            if types == "challengemention":
                serversettings['challengemention'] = channel.id
                allserversetting.update_one({
                    "_id": str(ctx.guild.id)
                }, {
                    "$set": {
                        "challengemention": serversettings['challengemention']
                    }
                })
                await ctx.channel.send("更改完畢")
        else:
            await ctx.channel.send("請先建立空白設定檔！")

    # 監聽器
    @commands.Cog.listener()
    async def on_guild_join(self, guild):
        serverid = str(guild.id)
        toinsert = {}
        toinsert['_id'] = serverid
        toinsert['welcomechannel'] = "未設定"
        toinsert['leavechannel'] = "未設定"
        toinsert['challengechannel'] = "未設定"
        toinsert['challengemention'] = "未設定"
        toinsert['suggestionchannel'] = "未設定"
        toinsert['funchat'] = "未設定"
        toinsert['logchannel'] = "未設定"
        toinsert['gamequestion'] = False
        toinsert['normalquestion'] = False
        toinsert['announcechannel'] = "未設定"
        toinsert['starboardchannel'] = "未設定"
        toinsert['poststar'] = 0
        toinsert['levelmessagechannel'] = "未設定"
        allserversetting.insert_one(toinsert)


def setup(bot):
    bot.add_cog(ChangeBotServerSettings(bot))
