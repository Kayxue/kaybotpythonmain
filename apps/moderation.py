import discord
import json
from discord.ext import commands
from core.classes import *
from typing import Union

with open("JSON/serversettings.json", mode="r", encoding="utf8") as jfile:
    serversettings = json.load(jfile)


class Moderation(Cog_Extension):
    @commands.command()
    @commands.has_permissions(kick_members=True)
    async def kick(self, ctx, kickuser: Union[discord.Member, int], *, reason: str = "未填寫"):
        channel = None
        try:
            channel = self.bot.get_channel(
                int(serversettings[str(ctx.guild.id)]['logchannel']))
        except KeyError:
            pass
        if isinstance(kickuser, discord.Member):
            user = kickuser
        elif isinstance(kickuser, int):
            user = await self.bot.fetch_user(kickuser)
        if not (channel == None):
            embed1 = discord.Embed(
                title='踢出成員', description='指定成員已成功被踢出！', color=self.successfulembedcolor)
            embed1.add_field(name='踢出成員：', value=user.mention, inline=True)
            embed1.add_field(
                name='執行者：', value=ctx.message.author.mention, inline=True)
            embed1.add_field(name='原因：', value=reason, inline=False)
            embed1.set_thumbnail(url=user.avatar_url)
            embed1.set_footer(text=ctx.message.author,
                              icon_url=ctx.message.author.avatar_url)
        await ctx.guild.kick(user=user, reason=reason)
        embed2 = discord.Embed(
            title='成功踢出成員！', description=f'已成功踢出成員：{user.mention}！', color=self.successfulembedcolor)
        if not (channel == None):
            await channel.send(embed=embed1)
        await ctx.channel.send(embed=embed2)

    @kick.error
    async def kick_error(self, ctx, error):
        if isinstance(error, discord.ext.commands.CheckFailure):
            embed1 = discord.Embed(
                title='權限不足！', description='您沒有權限執行此指令！', color=self.errorembedcolor)
            embed1.add_field(name='請確認您是否有以下權限：', value='踢出成員')
            await ctx.channel.send(embed=embed1)
        elif isinstance(error, discord.ext.commands.BadArgument):
            userID = ctx.message.author.id
            embed1 = discord.Embed(title='踢出失敗！', description='對不起，找不到此成員')
            await ctx.channel.send(embed=embed1)
        elif isinstance(error, discord.ext.commands.MissingRequiredArgument):
            embed1 = discord.Embed(
                title='請輸入要踢出之成員！', description='不會用嗎？沒關係，我幫你', color=self.normalembedcolor)
            embed1.add_field(
                name='用法', value='``s!kick [欲踢出之使用者的ID/提及] [原因（選用）]``')
            embed1.add_field(
                name='範例', value='``s!kick 983274927947329844 測試``')
            await ctx.channel.send(embed=embed1)
        else:
            embed1 = discord.Embed(
                title='踢出失敗！', description='對不起，無法踢出成員', color=self.errorembedcolor)
            embed1.add_field(name='錯誤訊息：', value=error)
            await ctx.channel.send(embed=embed1)

    @commands.command()
    @commands.has_permissions(ban_members=True)
    async def ban(self, ctx, banuser: Union[discord.Member, int], *, reason: str = "未填寫"):
        channel = None
        try:
            channel = self.bot.get_channel(
                int(serversettings[str(ctx.guild.id)]['logchannel']))
        except KeyError:
            pass
        user = ""
        if isinstance(banuser, discord.Member):
            user = banuser
        elif isinstance(banuser, int):
            user = await self.bot.fetch_user(banuser)
        if not (channel == None):
            embed1 = discord.Embed(
                title='封鎖成員', description='指定成員已成功被封鎖！', color=self.successfulembedcolor)
            embed1.add_field(name='封鎖成員：', value=user.mention, inline=True)
            embed1.add_field(
                name='執行者：', value=ctx.message.author.mention, inline=True)
            embed1.add_field(name='原因：', value=reason, inline=False)
            embed1.set_thumbnail(url=user.avatar_url)
            embed1.set_footer(text=ctx.message.author,
                              icon_url=ctx.message.author.avatar_url)
        await ctx.guild.ban(user=user, reason=reason, delete_message_days=7)
        embed2 = discord.Embed(
            title='成功封鎖成員！', description=f'已成功封鎖成員：{user.mention}！', color=self.successfulembedcolor)
        if not (channel == None):
            await channel.send(embed=embed1)
        await ctx.channel.send(embed=embed2)

    @ban.error
    async def ban_error(self, ctx, error):
        if isinstance(error, discord.ext.commands.CheckFailure):
            embed1 = discord.Embed(
                title='權限不足！', description='您沒有權限執行此指令！', color=self.errorembedcolor)
            embed1.add_field(name='請確認您是否有以下權限：', value='封鎖成員')
            await ctx.channel.send(embed=embed1)
        elif isinstance(error, discord.ext.commands.BadArgument):
            userID = ctx.message.author.id
            embed1 = discord.Embed(title='封鎖失敗！', description='對不起，找不到此成員')
            await ctx.channel.send(embed=embed1)
        elif isinstance(error, discord.ext.commands.MissingRequiredArgument):
            embed1 = discord.Embed(
                title='請輸入要封鎖之成員！', description='不會用嗎？沒關係，我幫你', color=self.normalembedcolor)
            embed1.add_field(
                name='用法', value='``s!ban [欲封鎖之使用者的ID/提及] [原因（選用）]``')
            embed1.add_field(
                name='範例', value='``s!ban 983274927947329844 測試``')
            await ctx.channel.send(embed=embed1)
        else:
            embed1 = discord.Embed(
                title='封鎖失敗！', description='對不起，無法封鎖成員', color=self.errorembedcolor)
            embed1.add_field(name='錯誤訊息：', value=error)
            await ctx.channel.send(embed=embed1)


def setup(bot):
    bot.add_cog(Moderation(bot))
