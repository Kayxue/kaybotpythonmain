import discord
from discord.ext import commands
from core.classes import *
import datetime


class NewsAndAnnounce(Cog_Extension):
    @commands.command()
    async def zutekbotnews(self, ctx, *, info: str):
        time = datetime.datetime.now().strftime("%Y/%m/%d")
        embed1 = discord.Embed(title=f"Zutek Bot更新日誌（{time}）", color=0xFFF780)
        outfield = []
        isend = False
        while True:
            await ctx.channel.send()
        role = discord.utils.get(ctx.guild.roles, name="Zutek Bot News")
        channel = self.bot.get_channel(647645780206813184)
        await channel.send(content=role.mention, embed=embed1)
        await ctx.channel.send("新聞傳送完成！")


def setup(bot):
    bot.add_cog(NewsAndAnnounce(bot))
