import discord
import asyncio
from discord.ext import commands
from core.classes import *
import json
import random

with open('JSON/setting.json', 'r', encoding='utf8') as jfile:
    jdata = json.load(jfile)


class Fun(Cog_Extension):
    @commands.command()
    async def say(self, ctx, *, msg: str):
        if str(msg) not in jdata['AUTORESPONDKEYWORD']:
            await ctx.channel.send(msg)
        elif ctx.channel.name == '和湯圓說說話':
            await ctx.channel.send(f'{ctx.author.mention}：{msg}')

    @commands.command()
    async def saydel(self, ctx, *, msg):
        await ctx.message.delete()
        if str(msg) not in jdata['AUTORESPONDKEYWORD']:
            await ctx.channel.send(msg)
        elif ctx.channel.name == '和湯圓說說話':
            await ctx.channel.send(f'{ctx.author.mention}：{msg}')

    @commands.command()
    async def loli(self, ctx):
        embed1 = discord.Embed(title='', description='',
                               color=self.normalembedcolor)
        embed1.set_image(url=random.choice(jdata['LOLYPICTURE']))
        await ctx.channel.send(embed=embed1)

    @commands.command()
    async def maketeam(self, ctx, howmanyinateam: int = 0, team: int = 0):
        if howmanyinateam >= 2:
            servermember = ctx.guild.members
            teamok = True
            onlines = []
            outputstr = ""
            teams = {}
            dotimes = 0
            for mem in servermember:
                if str(mem.status) == 'online':
                    if not mem.bot:
                        onlines.append(mem)

            if team == 0:
                dotimes = int(len(onlines) / howmanyinateam)
            else:
                if not team > int(len(onlines) / howmanyinateam):
                    dotimes = team
                else:
                    await ctx.channel.send("無法分出指定隊數！")
                    teamok = True

            if not dotimes < 1:
                for i in range(0, dotimes):  # while i<dotimes:
                    listtitle = f"第{i+1}小隊成員："
                    teams[listtitle] = []
                    # j=1
                    # while j<=howmanyinateam:
                    for j in range(1, howmanyinateam+1):
                        memb = random.choice(onlines)
                        teams[listtitle].append(memb)
                        delindex = onlines.index(memb)
                        del onlines[delindex]
                        #j += 1
                    #i += 1

                # l=0
                embed1 = discord.Embed(
                    title="組隊結果", description="以下為組隊結果", color=self.normalembedcolor)

                for l in range(0, dotimes):  # while l<dotimes:
                    outtitle = f"第{l+1}小隊成員："
                    outvalue = ""
                    for mem in teams[outtitle]:
                        outvalue += f"{mem}\n"
                    embed1.add_field(
                        name=outtitle, value=f"{outvalue}", inline=False)
                    l += 1

                await ctx.channel.send(embed=embed1)
            elif dotimes < 1 and teamok == True and team != 0:
                await ctx.channel.send(f"上線人數過少，無法分出{team}組並且一組{howmanyinateam}人")
            elif dotimes < 1 and teamok == True and team == 0:
                await ctx.channel.send(f"上線人數過少，無法分出一組{howmanyinateam}人")
            elif dotimes < 1 and teamok == False and team == 0:
                await ctx.channel.send(f"上線人數過少，無法分出一組{howmanyinateam}人")

        else:
            await ctx.channel.send(f'請輸入一隊有幾人（此數字必須大於等於2）')

    @commands.command()
    async def choose(self, ctx, *, allstrs: str):
        list = allstrs.split(' ')
        outputstr = random.choice(list)
        await ctx.channel.send(f"選擇結果：{outputstr}")

    @commands.command(name="random")
    async def randomint(self, ctx, mins: int, maxs: int):
        outputnum = random.randint(mins, maxs)
        await ctx.channel.send(f"結果：{outputnum}")

    @commands.command()
    async def calculate(self, ctx, input: str):
        await ctx.channel.send(eval(input))

    @commands.command()
    async def guessthenumber(self, ctx):
        answer = random.randint(1, 99)
        min, max = 0, 100
        startembed = discord.Embed(
            title="Guess a number", description=f"{min}/{max}", color=self.normalembedcolor).set_footer(text="Please answer in 30s")
        outmessage = await ctx.channel.send(embed=startembed)
        while True:
            try:
                def check(m):
                    return m.author == ctx.message.author

                msg = await self.bot.wait_for('message', timeout=30, check=check)
            except asyncio.TimeoutError:
                outmessage.edit(embed=discord.Embed(
                    title="Time out", description="I can't get your answer in 30s", color=self.errorembedcolor))
                return
            except ValueError:
                await ctx.channel.send(delete_after=3, content="please input an integer")
            else:
                if int(msg.content) > answer:
                    await msg.delete()
                    max = int(msg.content)
                    await outmessage.edit(content="your answer is bigger than answer", embed=discord.Embed(
                        title="Guess a number", description=f"{min}/{max}", color=self.errorembedcolor).set_footer(text="Please answer in 30s"))
                elif int(msg.content) < answer:
                    await msg.delete()
                    min = int(msg.content)
                    await outmessage.edit(content="your answer is less than answer", embed=discord.Embed(
                        title="Guess a number", description=f"{min}/{max}", color=self.errorembedcolor).set_footer(text="Please answer in 30s"))
                elif int(msg.content) == answer:
                    await msg.delete()
                    await outmessage.edit(embed=discord.Embed(
                        title="You got it", description=f"The answer is {answer}", color=self.successfulembedcolor))
                    return

    @guessthenumber.error
    async def guessthenumber_error(self, ctx, error):
        await ctx.channel.send(error)


def setup(bot):
    bot.add_cog(Fun(bot))
