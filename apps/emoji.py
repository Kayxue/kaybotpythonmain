import discord
import random
from discord.ext import commands
from core.classes import *


class Emoji(Cog_Extension):
    @commands.command(name="nitroemoji")
    async def nitroemoji(self, ctx, emoji: str):
        emojilist = self.bot.emojis
        outputemo = ""
        for emojis in emojilist:
            if emojis.name == emoji:
                outputemo = emojis
        if not outputemo == "":
            await ctx.channel.send(outputemo)
        else:
            await ctx.channel.send(f"找不到表情符號「{emoji}」")

    @commands.command(name="nitroemojidel")
    async def nitroemojidel(self, ctx, emoji: str):
        emojilist = self.bot.emojis
        outputemo = ""
        for emojis in emojilist:
            if emojis.name == emoji:
                outputemo = emojis
        await ctx.message.delete()
        if not outputemo == "":
            await ctx.channel.send(outputemo)
        else:
            await ctx.channel.send(f"找不到表情符號「{emoji}」")


def setup(bot):
    bot.add_cog(Emoji(bot))
