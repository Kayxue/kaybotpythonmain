import json
import os


def save(idd, data):
    if os.path.isfile("users.json"):
        with open('users.json', 'r') as f:
            users = json.load(f)
    with open('users.json', 'w') as f:
        try:
            type(users)
        except:
            users = {}
        users[str(idd)] = data
        json.dump(users, f, sort_keys=True, indent=4, ensure_ascii=False)


save(1234567890, "test")
